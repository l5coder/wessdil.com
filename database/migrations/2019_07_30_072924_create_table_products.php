<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->longText('slug');
            $table->uuid('vendor_uuid');
            $table->string('title',300);
            $table->uuid('category_uuid');
            $table->uuid('destination_uuid');
            $table->string('cover',300);
            $table->longText('gallery');
            $table->longText('included_uuid');
            $table->longText('exclude_uuid');
            $table->longText('facilities');
            $table->longText('description');
            $table->longText('itinerary')->nullable();
            $table->longText('highlights');
            $table->longText('price');
            $table->primary('uuid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
