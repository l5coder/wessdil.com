<?php
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $userLevel = new \App\Models\UserLevelModel();
//        $userLevel->level_name = 'admin';
//        $userLevel->is_active = true;
//        $userLevel->save();
//
//        $userLevel = new \App\Models\UserLevelModel();
//        $userLevel->level_name = 'vendor';
//        $userLevel->is_active = true;
//        $userLevel->save();
//
//
//        $user = new \App\Models\User();
//        $user->name = 'admin';
//        $user->email = 'admin@admin.com';
//        $user->password = bcrypt('adminD3v');
//        $user->user_level_uuid = 1;
//        $user->created_at = date('Y-m-d H:i:s');
//        $user->save();
//
//
//        $user = new \App\Models\User();
//        $user->name = 'vendor';
//        $user->email = 'vendor@vendor.com';
//        $user->password = bcrypt('vendorD3v');
//        $user->user_level_uuid = 2;
//        $user->created_at = date('Y-m-d H:i:s');
//        $user->save();

        $level = new \App\Models\UserLevelModel();
        $level->level_name = 'Super Admin';
        $level->is_active = 1;
        $level->save();


        $level = new \App\Models\UserLevelModel();
        $level->level_name = 'Admin';
        $level->is_active = 1;
        $level->save();

        $level = new \App\Models\UserLevelModel();
        $level->level_name = 'Vendor';
        $level->is_active = 1;
        $level->save();

        $level = new \App\Models\UserLevelModel();
        $level->level_name = 'Member';
        $level->is_active = 1;
        $level->save();

    }
}
