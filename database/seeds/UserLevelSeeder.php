<?php

use Illuminate\Database\Seeder;

class UserLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::insert([
            'level_name'=>'Super Admin',
            'is_active'=>1
        ]);

        \Illuminate\Support\Facades\DB::insert([
            'level_name'=>'Admin',
            'is_active'=>1
        ]);

        \Illuminate\Support\Facades\DB::insert([
            'level_name'=>'Vendor',
            'is_active'=>1
        ]);

        \Illuminate\Support\Facades\DB::insert([
            'level_name'=>'Member',
            'is_active'=>1
        ]);
    }
}
