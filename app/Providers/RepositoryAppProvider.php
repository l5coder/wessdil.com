<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryAppProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\\Repositories\\Contracts\\IUserRepository', 'App\\Repositories\\Actions\UserRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IUserLevelRepository', 'App\\Repositories\\Actions\UserLevelRepository');
        $this->app->bind('App\\Repositories\\Contracts\\ICitiesRepository', 'App\\Repositories\\Actions\CitiesRepository');
        $this->app->bind('App\\Repositories\\Contracts\\ITourPackageCategoriesRepository', 'App\\Repositories\\Actions\TourPackageCategoriesRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IToursRepository', 'App\\Repositories\\Actions\ToursRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IRolesRepository', 'App\\Repositories\\Actions\RolesRepository');
        $this->app->bind('App\\Repositories\\Contracts\\ICustomersRepository', 'App\\Repositories\\Actions\CustomersRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IRoleScopeRepository', 'App\\Repositories\\Actions\RoleScopeRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IRoleUserRepository', 'App\\Repositories\\Actions\RoleUserRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IScopesRepository', 'App\\Repositories\\Actions\ScopesRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IVendorPicsRepository', 'App\\Repositories\\Actions\VendorPicsRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IVendorsRepository', 'App\\Repositories\\Actions\VendorsRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IDestinationRepository', 'App\\Repositories\\Actions\DestinationRepository');
        $this->app->bind('App\\Repositories\\Contracts\\ICategoryRepository', 'App\\Repositories\\Actions\CategoryRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IProductRepository', 'App\\Repositories\\Actions\ProductRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IProductCategoryGroupsRepository', 'App\\Repositories\\Actions\ProductCategoryGroupsRepository');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
