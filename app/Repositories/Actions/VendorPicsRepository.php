<?php
/**
 * Created by PhpStorm.
 * User: klf
 * Date: 20/06/2019
 * Time: 22:05
 */

namespace App\Repositories\Actions;


use App\Models\VendorPicsModel;
use App\Repositories\Contracts\IVendorPicsRepository;

class VendorPicsRepository implements IVendorPicsRepository
{
    public function create($input)
    {
        $vendor_pics = new VendorPicsModel();
        $vendor_pics->vendor_uuid = $input['vendor_uuid'];
        $vendor_pics->customer_uuid = $input['customer_uuid'];
        $vendor_pics->name = $input['name'];
        $vendor_pics->email = $input['email'];
        $vendor_pics->mobile = $input['mobile'];

        return $vendor_pics->uuid;
    }

    public function read($id)
    {
        $vendor_pic = VendorPicsModel::uuid($id);

        return[
            'vendor_uuid'=>$vendor_pic->vendor_uuid,
            'customer_uuid'=>$vendor_pic->customer_uuid,
            'name'=>$vendor_pic->name,
            'email'=>$vendor_pic->email,
            'mobile'=>$vendor_pic->mobile,
        ];
    }

    public function update($input)
    {
        $vendor_pics = VendorPicsModel::uuid($input['uuid']);
        $vendor_pics->vendor_uuid = $input['vendor_uuid'];
        $vendor_pics->customer_uuid = $input['customer_uuid'];
        $vendor_pics->name = $input['name'];
        $vendor_pics->email = $input['email'];
        $vendor_pics->mobile = $input['mobile'];

        return $vendor_pics->save();
    }

    public function delete($id)
    {
        return VendorPicsModel::uuid($id)->delete();
    }

    public function showAll($search = null)
    {
        $data = [];
        $query = VendorPicsModel::query();

        if($search!= null){
            $query->where( 'vendor_pics','like',$search.'%');
        }

        $vendor_pics = $query->paginate(10);

        foreach ($vendor_pics as $vendor_pic);
        $data[]=[
            'vendor_uuid'=>$vendor_pic->vendor_uuid,
            'customer_uuid'=>$vendor_pic->customer_uuid,
            'name'=>$vendor_pic->name,
            'email'=>$vendor_pic->email,
            'mobile'=>$vendor_pic->mobile,
            ];
    }

    public function pagination($page = 1, $rows = 10, $sortBy = 'uuid', $order = 'asc', $search = null, $filter = null)
    {
        // TODO: Implement pagination() method.
    }


    public function isVendorPicNameExist($vendor_picName, $uuid = null)
    {
        $query = VendorPicsModel::query();
        $query->where('vendor_pic','=',$vendor_picName);
        if ($uuid!= null){
            $query->where('uuid', '<>',$uuid);
        }

        return ($query->count()>0);
    }
}