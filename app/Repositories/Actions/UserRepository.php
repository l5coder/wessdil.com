<?php

namespace App\Repositories\Actions;


use App\Models\User;
use App\Repositories\Contracts\IUserRepository;
use App\Repositories\Contracts\Pagination\PaginationParam;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserRepository implements IUserRepository
{
    public function create($input)
    {
        $user = new User();
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);
        $user->user_level_uuid = $input['user_level_uuid'];
        $user->is_active = $input['is_active'];
        $user->save();

        return $user->uuid;
    }

    public function update($input)
    {
        $user = User::find($input['id']);
        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->user_level_uuid = $input['user_level_uuid'];
        $user->is_active = $input['is_active'];

        return $user->save();
    }

    public function delete($id)
    {
        return User::find($id)->delete();
    }

    public function read($id)
    {
        $user = User::uuid($id);

        return [
            'uuid'=>$user->uuid,
            'name'=>$user->name,
            'email'=>$user->email,
            'user_level_uuid'=>$user->user_level_uuid,
            'is_active'=>$user->is_active
        ];
    }

    public function showAll($search = null)
    {
        $users = User::all();
        $data = [];

        foreach ($users as $user){
            $data[]=[
                'uuid'=>$user->uuid,
                'name'=>$user->name,
                'email'=>$user->email,
                'user_level_uuid'=>$user->user_level_uuid,
                'is_active'=>$user->is_active
            ];
        }

        return $data;
    }


    public function pagination($searchPhrase = null)
    {

    }

    public function isUserNameExist($email, $uuid = null)
    {
        if($uuid == null){
            $result = User::where('email','=',$email)->count();
        }else{
            $result = User::where('email','=',$email)->where('uuid','<>',$uuid)->count();
        }

        return ($result>0);
    }

    public function changePassword($uuid, $password)
    {
        return User::where('uuiud','=',$uuid)->update(['password'=>bcrypt($password)]);
    }

    public function changeUserEmail($uuid, $email)
    {
        $user = User::find($uuid);
        $user->email = $email;

        return $user->save();
    }

    public function deleteByEmail($email)
    {
        return User::where('email','=',$email)->delete();
    }


}
