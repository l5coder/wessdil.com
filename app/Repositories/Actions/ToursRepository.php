<?php

namespace App\Repositories\Actions;


use App\Models\TourPackageCategoriesModel;
use App\Models\ToursModel;
use App\Repositories\Contracts\IToursRepository;

class ToursRepository implements IToursRepository
{

    public function create($input)
    {
        $tour = new ToursModel();
        $tour->user_uuid = $input['user_uuid'];
        $tour->tour_package_category_uuid = $input['tour_package_category_uuid'];
        $tour->slug = $input['slug'];
        $tour->title = $input['title'];
        $tour->valid_until = $input['valid_until'];
        $tour->location = $input['location'];
        $tour->location_type = $input['location_type'];
        $tour->itinerary = $input['itinerary'];
        $tour->term_of_service = $input['term_of_service'];
        $tour->cover_image = $input['cover_image'];
        $tour->gallery = $input['gallery'];
        $tour->price = $input['price'];
        $tour->created_at = date('Y-m-d H:i:s');

        return $tour->save();
    }

    public function update($input)
    {
        $tour = ToursModel::find($input['uuid']);
        $tour->user_uuid = $input['user_uuid'];
        $tour->tour_package_category_uuid = $input['tour_package_category_uuid'];
        $tour->slug = $input['slug'];
        $tour->title = $input['title'];
        $tour->valid_until = $input['valid_until'];
        $tour->location = $input['location'];
        $tour->location_type = $input['location_type'];
        $tour->itinerary = $input['itinerary'];
        $tour->term_of_service = $input['term_of_service'];
        $tour->cover_image = $input['cover_image'];
        $tour->gallery = $input['gallery'];
        $tour->price = $input['price'];
        $tour->created_at = date('Y-m-d H:i:s');

        return $tour->save();
    }

    public function delete($id)
    {
        return ToursModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $tour = ToursModel::uuid($id);

        return [
            'uuid'=>$tour->uuid,
            'user_uuid'=>$tour->user_uuid,
            'tour_package_category_uuid'=>$tour->tour_package_category_uuid,
            'title'=>$tour->title,
            'valid_until'=>$tour->valid_until,
            'location'=>$tour->location,
            'location_type'=>$tour->location_type,
            'itinerary'=>$tour->itinerary,
            'term_of_service'=>json_decode($tour->term_of_service),
            'cover_image'=>$tour->cover_image,
            'gallery'=>$tour->gallery,
            'price'=>$tour->price
        ];
    }

    public function showAll($search = null)
    {
        $tours = ToursModel::all();
        $data = [];

        foreach ($tours as $tour){
            $data[]=[
                'uuid'=>$tour->uuid,
                'user_uuid'=>$tour->user_uuid,
                'tour_package_category_uuid'=>$tour->tour_package_category_uuid,
                'title'=>$tour->title,
                'valid_until'=>$tour->valid_until,
                'location'=>$tour->location,
                'location_type'=>$tour->location_type,
                'itinerary'=>$tour->itinerary,
                'term_of_service'=>json_decode($tour->term_of_service),
                'cover_image'=>$tour->cover_image,
                'gallery'=>$tour->gallery,
                'price'=>$tour->price
            ];
        }

        return $data;
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function showPagingFrontOffice($sorting,$search = null, $filter = null, $priceRange = null)
    {
        $query = ToursModel::query();

        if($search != null){
            $query->where(function($q)use($search){
                $q->where('title','like',$search.'%')
                    ->orWhere('itinerary','like',$search.'%');
            });
        }

        if($filter != null){
            if($filter['location'] != null){
                $query->orWhereIn('location',$filter['location']);
            }
        }

        if($priceRange != null){
            $query->orWhereBetween('price',$priceRange);
        }

        $query->orderBy($sorting['sortBy'],$sorting['sortOrder']);
        $oneDayTours = $query->paginate(12);

        return $oneDayTours;
    }


}