<?php
/**
 * Created by PhpStorm.
 * User: klf
 * Date: 18/06/2019
 * Time: 13:08
 */

namespace App\Repositories\Actions;


use App\Models\RolesModel;
use App\Models\ScopesModel;
use App\Repositories\Contracts\IScopesRepository;

class ScopesRepository implements IScopesRepository
{
    public function create($input)
    {
        $scope = new ScopesModel();
        $scope->scope = $input['scope'];
        $scope->description = $input['description'];

        return $scope->save();
    }

    public function read($id)
    {
        $scope = ScopesModel::uuid($id);

        return[
            'uuid'=>$scope->uuid,
            'scope'=>$scope->scope,
            'description'=>$scope->description
        ];
    }

    public function update($input)
    {
        $scope = ScopesModel::uuid($input['uuid']);
        $scope->scope = $input['scope'];
        $scope->description = $input['description'];

        return $scope->save();
    }

    public function delete($id)
    {
        return ScopesModel::uuid($id)->delete();
    }

    public function showAll()
    {

    }

    public function pagination($page = 1, $rows = 10, $sortBy = '', $order = 'asc', $search = null, $filter = null)
    {
        $data = [];
        $query = ScopesModel::query();
        $count = $query->count();
        $skip = ($page-1)*$rows;

        if($search!= null){
            $query->where( 'scope','like',$search.'%');
        }

        $scopes = $query->skip($skip)->paginate($rows);
        foreach ($scopes as $scope){
            $data[]=[
                'uuid'=>$scope->uuid,
                'scope'=>$scope->scope,
                'description'=>$scope->description
            ];
        }

        $responsePagination = [
            'per_page'=>$rows,
            'sort_by'=>$sortBy,
            'sort_desc'=>($order == 'desc' ? true:false),
            'current_page'=>$page,
            'total'=>$count,
            'items'=>$data
        ];

        return $responsePagination;
    }


    public function isScopeNameExist($scopeName, $uuid = null)
    {
        $query = ScopesModel::query();
        $query->where('scope','=',$scopeName);
        if ($uuid!= null){
            $query->where('uuid', '<>',$uuid);
        }

        return ($query->count()>0);
    }
}