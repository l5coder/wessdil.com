<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-07-27
 * Time: 19:34
 */

namespace App\Repositories\Actions;


use App\Models\CategoriesModel;
use App\Repositories\Contracts\ICategoryRepository;
use App\Repositories\Contracts\Pagination\PaginationParam;

class CategoryRepository implements ICategoryRepository
{

    public function create($input)
    {
        $category = new CategoriesModel();
        $category->name = $input['name'];
        $category->parent_uuid = $input['parent_uuid'];
        $category->is_active = $input['is_active'];

        return $category->save();
    }

    public function update($input)
    {
        $category = CategoriesModel::find($input['uuid']);
        $category->name = $input['name'];
        $category->parent_uuid = $input['parent_uuid'];
        $category->is_active = $input['is_active'];

        return $category->save();
    }

    public function delete($id)
    {
        return CategoriesModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $category = CategoriesModel::uuid($id);

        return [
            'uuid'=>$category->uuid,
            'name'=>$category->name,
            'parent_uuid'=>$category->parent_uuid,
            'is_active'=>($category->is_active == 1 ? true:false)
        ];
    }

    public function showAll($search = null)
    {
        $categories = CategoriesModel::all();
        $data = [];

        foreach ($categories as $category){
            $data[]=[
                'uuid'=>$category->uuid,
                'name'=>$category->name,
                'is_active'=>($category->is_active == 1 ? 'Yes':'No')
            ];
        }

        return $data;
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function isCategoryExist($categoryName, $uuid = null)
    {
        if($uuid == null){
            $result = CategoriesModel::where('name','=',$categoryName)->count();
        }else{
            $result = CategoriesModel::where('name','=',$categoryName)->where('uuid','<>',$uuid)->count();
        }

        return ($result > 0);
    }

    public function categoryTreeview($parentUuid = null)
    {
        $categories = CategoriesModel::whereNull('parent_uuid')->orderBy('uuid','asc')
            ->get();
        if($parentUuid != null){
            $categories = CategoriesModel::where('parent_uuid','=',$parentUuid)
                ->orderBy('uuid','asc')
                ->get();
        }


        $data = [];
        if(count($categories)>0){
            foreach ($categories as $category){
                $childs = $this->categoryTreeview($category->uuid);
                if(count($childs)>0){
                    $data[] = [
                        'value'=>$category->uuid,
                        'text'=>$category->name,
                        'children'=>$childs,
                        'opened'=>true
                    ];
                }else{
                    $data[] = [
                        'value'=>$category->uuid,
                        'text'=>$category->name,
                    ];
                }

            }
        }

        return $data;
    }

    public function isHaveChildren($uuid)
    {
        $result = CategoriesModel::where('parent_uuid','=',$uuid)->count();

        return ($result > 0);
    }

    public function deleteChildren($parentUuid)
    {
        return CategoriesModel::where('parent_uuid','=',$parentUuid)->delete();
    }

    public function getChildren($parentUuid)
    {
        $childrens = CategoriesModel::where('parent_uuid','=',$parentUuid)->get();
        $data= [];

        foreach ($childrens as $children){
            $data[]=[
                'uuid'=>$children->uuid,
                'name'=>$children->name
            ];
        }

        return $data;
    }


}