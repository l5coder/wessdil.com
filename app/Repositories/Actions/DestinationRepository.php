<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-07-27
 * Time: 13:43
 */

namespace App\Repositories\Actions;


use App\Models\DestinationsModel;
use App\Repositories\Contracts\IDestinationRepository;

class DestinationRepository implements IDestinationRepository
{

    public function create($input)
    {
        $destinations = new DestinationsModel();
        $destinations->name = $input['name'];
        $destinations->slug = $input['slug'];
        $destinations->description = $input['description'];
        $destinations->type = $input['type'];
        $destinations->cover_image = $input['cover_image'];
//        $destinations->thumbnail_cover = $input['thumbnail_cover'];
        $destinations->is_active = $input['is_active'];

        return $destinations->save();
    }

    public function update($input)
    {
        $destinations = DestinationsModel::find($input['uuid']);
        $destinations->name = $input['name'];
        $destinations->type = $input['type'];
        $destinations->slug = $input['slug'];
        $destinations->description = $input['description'];
        $destinations->cover_image = $input['cover_image'];
//        $destinations->thumbnail_cover = $input['thumbnail_cover'];
        $destinations->is_active = $input['is_active'];

        return $destinations->save();
    }

    public function delete($id)
    {
        return DestinationsModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $destination = DestinationsModel::where('uuid','=',$id)->orWhere('slug','=',$id)->first();

        return [
            'uuid'=>$destination->uuid,
            'name'=>$destination->name,
            'slug'=>$destination->slug,
            'type'=>$destination->type,
            'description'=>$destination->description,
            'cover_image'=>$destination->cover_image,
            'thumbnail_cover'=>$destination->thumbnail_cover,
            'is_active'=>($destination->is_active == 1 ? true:false)
        ];
    }

    public function showAll($search = null)
    {
        $destinations = DestinationsModel::all();
        $data = [];

        foreach ($destinations as $destination){
            $data[]= [
                'uuid'=>$destination->uuid,
                'name'=>$destination->name,
                'type'=>$destination->type,
                'slug'=>$destination->slug,
                'description'=>$destination->description,
                'cover_image'=>$destination->cover_image,
                'thumbnail_cover'=>$destination->thumbnail_cover,
                'is_active'=>($destination->is_active ? 'Yes':'No')
            ];
        }

        return $data;
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function isDestinationExist($destinationName, $uuid = null)
    {
       if($uuid == null){
           $result = DestinationsModel::where('name','=',$destinationName)->count();
       }else{
           $result = DestinationsModel::where('name','=',$destinationName)->where('uuid','<>',$uuid)->count();
       }

       return ($result>0);
    }
}