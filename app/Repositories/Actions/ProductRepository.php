<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-07-30
 * Time: 15:15
 */

namespace App\Repositories\Actions;


use App\Models\ProductsModel;
use App\Repositories\Contracts\IProductRepository;

class ProductRepository implements IProductRepository
{

    public function create($input)
    {
        $product = new ProductsModel();
        $product->slug = $input['slug'];
        $product->vendor_uuid = $input['vendor_uuid'];
        $product->title = $input['title'];
        $product->destination_uuid = $input['destination_uuid'];
        $product->cover = $input['cover'];
        $product->gallery = $input['gallery'];
        $product->include = $input['include'];
        $product->exclude = $input['exclude'];
        $product->facilities = $input['facilities'];
        $product->description = $input['description'];
        $product->itinerary = $input['itinerary'];
        $product->highlights = $input['highlights'];
        $product->price = $input['price'];
        $product->is_have_itinerary = $input['isHaveItinerary'];
        $product->save();

        return $product->uuid;
    }

    public function update($input)
    {
        $product = ProductsModel::find($input['uuid']);
        $product->slug = $input['slug'];
        $product->vendor_uuid = $input['vendor_uuid'];
        $product->title = $input['title'];
        $product->destination_uuid = $input['destination_uuid'];
        $product->cover = $input['cover'];
        $product->gallery = $input['gallery'];
        $product->include = $input['include'];
        $product->exclude = $input['exclude'];
        $product->facilities = $input['facilities'];
        $product->description = $input['description'];
        $product->itinerary = $input['itinerary'];
        $product->highlights = $input['highlights'];
        $product->price = $input['price'];
        $product->is_have_itinerary = $input['isHaveItinerary'];

        return $product->save();
    }

    public function delete($id)
    {
        return ProductsModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $product = ProductsModel::join('vendors','vendors.uuid','=','products.vendor_uuid')
            ->join('destinations','destinations.uuid','=','products.destination_uuid')
            ->select('products.*','destinations.name as destination_name','destinations.slug as destination_slug',
                'vendors.name as vendor_name')
            ->where('products.uuid','=',$id)
            ->orWhere('products.slug','=',$id)
            ->first();

        return [
            'uuid'=>$product->uuid,
            'slug'=>$product->slug,
            'vendor_uuid'=>$product->vendor_uuid,
            'vendor'=>$product->vendor_name,
            'title'=>$product->title,
            'destination_uuid'=>$product->destination_uuid,
            'destination'=>$product->destination_name,
            'destination_slug'=>$product->destination_slug,
            'cover'=>$product->cover,
            'gallery'=>$product->gallery,
            'include'=>$product->include,
            'exclude'=>$product->exclude,
            'facilities'=>$product->facilities,
            'description'=>$product->description,
            'itinerary'=>$product->itinerary,
            'highlights'=>$product->highlights,
            'price'=>$product->price,
            'isHaveItinerary'=>$product->is_have_itinerary
        ];
    }

    public function showAll($search = null)
    {
        $products = ProductsModel::join('vendors','vendors.uuid','=','products.vendor_uuid')
            ->join('destinations','destinations.uuid','=','products.destination_uuid')
            ->select('products.*','destinations.name as destination_name','destinations.slug as destination_slug',
                'vendors.name as vendor_name')
            ->get();
        $data = [];

        foreach ($products as $product){
            $data[]=[
                'uuid'=>$product->uuid,
                'slug'=>$product->slug,
                'vendor_uuid'=>$product->vendor_uuid,
                'vendor'=>$product->vendor_name,
                'title'=>$product->title,
                'destination_uuid'=>$product->destination_uuid,
                'destination'=>$product->destination_name,
                'destination_slug'=>$product->destination_slug,
                'cover'=>$product->cover,
                'gallery'=>$product->gallery,
                'include'=>$product->include,
                'exclude'=>$product->exclude,
                'facilities'=>$product->facilities,
                'description'=>$product->description,
                'itinerary'=>$product->itinerary,
                'highlights'=>$product->highlights,
                'price'=>$product->price,
                'isHaveItinerary'=>$product->is_have_itinerary
            ];
        }

        return $data;
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function isSlugExist($slug, $uuid = null)
    {
        if($uuid == null){
            $result = ProductsModel::where('slug','=',$slug)->count();
        }else{
            $result = ProductsModel::where('slug','=',$slug)->where('uuid','<>',$uuid)->count();
        }

        return ($result > 0);
    }

    public function getProductByDestination($destinationUuid)
    {
        $products = ProductsModel::join('vendors','vendors.uuid','=','products.vendor_uuid')
            ->join('destinations','destinations.uuid','=','products.destination_uuid')
            ->select('products.*','destinations.name as destination_name','destinations.slug as destination_slug',
                'vendors.name as vendor_name')
            ->where('destinations.slug','=',$destinationUuid)
            ->get();
        $data = [];

        foreach ($products as $product){
            $data[]=[
                'uuid'=>$product->uuid,
                'slug'=>$product->slug,
                'vendor_uuid'=>$product->vendor_uuid,
                'vendor'=>$product->vendor_name,
                'title'=>$product->title,
                'category_uuid'=>$product->category_uuid,
                'destination'=>$product->destination_name,
                'destination_slug'=>$product->destination_slug,
                'cover'=>$product->cover,
                'gallery'=>$product->gallery,
                'include'=>$product->include,
                'exclude'=>$product->exclude,
                'facilities'=>$product->facilities,
                'description'=>$product->description,
                'itinerary'=>$product->itinerary,
                'highlights'=>$product->highlights,
                'price'=>$product->price,
                'isHaveItinerary'=>$product->is_have_itinerary
            ];
        }

        return $data;
    }


}