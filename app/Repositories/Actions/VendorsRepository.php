<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 9:40 PM
 */

namespace App\Repositories\Actions;


use App\Models\VendorsModel;
use App\Repositories\Contracts\IVendorsRepository;

class VendorsRepository implements IVendorsRepository
{
    public function create($input)
    {
        $vendor = new VendorsModel();
        $vendor->user_uuid = $input['user_uuid'];
        $vendor->name = $input['name'];
        $vendor->email = $input['email'];
        $vendor->phone = $input['phone'];
        $vendor->vendor_type = $input['vendor_type'];
        $vendor->address = $input['address'];
        $vendor->is_active = $input['is_active'];

        return $vendor->save();
    }

    public function update($input)
    {
        $vendor = VendorsModel::find($input['uuid']);
        $vendor->name = $input['name'];
        $vendor->email = $input['email'];
        $vendor->phone = $input['phone'];
        $vendor->vendor_type = $input['vendor_type'];
        $vendor->address = $input['address'];
        $vendor->is_active = $input['is_active'];

        return $vendor->save();
    }

    public function delete($id)
    {
        return VendorsModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $vendor = VendorsModel::uuid($id);

        $mou = json_decode($vendor->mou);
        $data = [
            'uuid' => $vendor->uuid,
            'user_uuid'=>$vendor->user_uuid,
            'name' => $vendor->name,
            'email' => $vendor->email,
            'phone' => $vendor->phone,
            'vendor_type' => $vendor->vendor_type,
            'address' => $vendor->address,
            'is_active'=>($vendor->is_active == 1 ? true:false)
        ];

        return $data;
    }

    public function showAll($search = null)
    {
        $vendors = VendorsModel::all();
        $data = [];

        foreach ($vendors as $vendor){
            $data[]=[
                'uuid' => $vendor->uuid,
                'user_uuid'=>$vendor->user_uuid,
                'name' => $vendor->name,
                'email' => $vendor->email,
                'phone' => $vendor->phone,
                'vendor_type' => $vendor->vendor_type,
                'address' => $vendor->address,
                'is_active'=>($vendor->is_active == 1 ? 'Yes':'No')
            ];
        }

        return $data;
    }

    public function pagination($page = 1, $rows = 10, $sortBy = 'uuid', $order = 'asc', $search = null, $filter = null)
    {
        // TODO: Implement pagination() method.
    }


    public function isVendorNameExist($vendorName, $uuid = null)
    {
        if($uuid == null){
            $result = VendorsModel::where('name','=',$vendorName)->count();
        }else{
            $result = VendorsModel::where('name','=',$vendorName)->where('uuid','<>',$uuid)->count();
        }

        return ($result > 0);
    }



}