<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 4:12 AM
 */

namespace App\Repositories\Actions;


use App\Models\RoleUserModel;
use App\Repositories\Contracts\IRoleUserRepository;

class RoleUserRepository implements IRoleUserRepository
{
    public function create($input){
        $role_user = new RoleUserModel();
        $role_user->user_uuid = $input['user'];
        $role_user->role_uuid = $input['role'];

        return $role_user->save();
    }

    public function update($input){
        $role_user = RoleUserModel::uuid($input['uuid']);
        $role_user->user_uuid = $input['user'];
        $role_user->role_uuid = $input['role'];

        return $role_user->save();
    }

    public function read($id)
    {
        $role_user = RoleUserModel::uuid($id);

        return[
            'uuid'=>$role_user->uuid,
            'user'=>$role_user->user_uuid,
            'role'=>$role_user->role_uuid,
        ];
    }

    public function delete($id)
    {
        return RoleUserModel::uuid($id)->delete();
    }

    public function showAll($search = null){

    }

    public function pagination($page = 1, $rows = 10, $sortBy = 'uuid', $order = 'asc', $search = null, $filter = null)
    {
        // TODO: Implement pagination() method.
    }


    public function isRoleUserExist($roleUser, $uuid = null)
    {
        $query = RoleUserModel::query();
        $query->where('role_user','=',$roleUser);
        if ($uuid!= null){
            $query->where('uuid','<>',$uuid);
        }

        return ($query->count()>0);
    }
}