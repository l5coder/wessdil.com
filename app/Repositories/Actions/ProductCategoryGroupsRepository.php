<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-09-01
 * Time: 11:19
 */

namespace App\Repositories\Actions;


use App\Models\ProductCategoryGroupsModel;
use App\Repositories\Contracts\IProductCategoryGroupsRepository;

class ProductCategoryGroupsRepository implements IProductCategoryGroupsRepository
{

    public function create($input)
    {
        $productCategoryGroups = new ProductCategoryGroupsModel();
        $productCategoryGroups->product_uuid = $input['productUuid'];
        $productCategoryGroups->category_uuid = $input['categoryUuid'];

        return $productCategoryGroups->save();
    }

    public function update($input)
    {
        return ProductCategoryGroupsModel::where('product_uuid','=',$input['productUuid'])
            ->update([
                'category_uuid'=>$input['categoryUuid']
            ]);
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function read($id)
    {
        return ProductCategoryGroupsModel::where('product_uuid','=',$id)->get();
    }

    public function showAll($search = null)
    {
        // TODO: Implement showAll() method.
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function deleteCategory($productUuid,$categoryUuid)
    {
        return ProductCategoryGroupsModel::where('product_uuid','=',$productUuid)
            ->where('category_uuid','=',$categoryUuid)->delete();
    }

    public function deleteProduct($productUuid)
    {
        return ProductCategoryGroupsModel::where('product_uuid','=',$productUuid)->delete();
    }
}