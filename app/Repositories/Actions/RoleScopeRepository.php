<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 3:40 AM
 */

namespace App\Repositories\Actions;


use App\Models\RoleScopeModel;
use App\Repositories\Contracts\IRoleScopeRepository;

class RoleScopeRepository implements IRoleScopeRepository
{
    public function create($input){
        $role_scope = new RoleScopeModel();
        $role_scope->role_uuid = $input['role'];
        $role_scope->scope_uuid = $input['scope'];

        return $role_scope->save();
    }

    public function update($input){
        $role_scope = RoleScopeModel::uuid($input['uuid']);
        $role_scope->role_uuid = $input['role'];
        $role_scope->scope_uuid = $input['scope'];

        return $role_scope->save();
    }

    public function read($id)
    {
        $role_scope = RoleScopeModel::uuid($id);

        return[
            'uuid'=>$role_scope->uuid,
            'role'=>$role_scope->role_uuid,
            'scope'=>$role_scope->scope_uuid,
        ];
    }

    public function delete($id)
    {
        return RoleScopeModel::uuid($id)->delete();
    }

    public function showAll($page = 1, $rows = 10, $sortBy = 'uuid', $order = 'asc',$search = null,$filter = null){

    }

    public function pagination($page = 1, $rows = 10, $sortBy = 'uuid', $order = 'asc', $search = null, $filter = null)
    {

    }

    public function isRoleScopeExist($roleScope, $uuid = null)
    {
        $query = RoleScopeModel::query();
        $query->where('role_scope','=',$roleScope);
        if ($uuid!= null){
            $query->where('uuid','<>',$uuid);
        }

        return ($query->count()>0);
    }
}