<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-08
 * Time: 00:42
 */

namespace App\Repositories\Actions;


use App\Models\TourPackageCategoriesModel;
use App\Repositories\Contracts\ITourPackageCategoriesRepository;

class TourPackageCategoriesRepository implements ITourPackageCategoriesRepository
{

    public function create($input)
    {
        $tourPackageCategory = new TourPackageCategoriesModel();
        $tourPackageCategory->package_category = $input['package_category'];
        $tourPackageCategory->created_at = date('Y-m-d H:i:s');

        return $tourPackageCategory->save();
    }

    public function update($input)
    {
        return TourPackageCategoriesModel::uuid($input['uuid'])
            ->update([
                'package_category'=>$input['package_category']
            ]);
    }

    public function delete($id)
    {
       return TourPackageCategoriesModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $tourPackageCategory = TourPackageCategoriesModel::uuid($id);

        return [
            'uuid'=>$tourPackageCategory->uuid,
            'package_category'=>$tourPackageCategory->package_category
        ];
    }

    public function showAll($search = null)
    {
        $tourPackageCategories = TourPackageCategoriesModel::all();
        $data = [];

        foreach ($tourPackageCategories as $tourPackageCategory){
            $data[]=[
                'uuid'=>$tourPackageCategory->uuid,
                'package_category'=>$tourPackageCategory->package_category
            ];
        }

        return $data;
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function isTourPackageCategoryExist($packageCategory, $uuid = null)
    {
        $query = TourPackageCategoriesModel::query();

        $query->where('package_category','=',$packageCategory);
        if($uuid!=null){
            $query->where('uuid','<>',$uuid);
        }

        return ($query->count() > 0);
    }
}