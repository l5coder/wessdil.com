<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-10
 * Time: 15:10
 */

namespace App\Repositories\Actions;


use App\Repositories\Contracts\ICitiesRepository;
use App\Models\CitiesModel;

class CitiesRepository implements ICitiesRepository
{

    public function create($input)
    {
        $city = new CitiesModel();
        $city->city = $input['city'];
        $city->city_type = $input['city_type'];
        $city->country = $input['country'];
        $city->about = $input['about'];
        $city->cover_image = $input['cover_image'];

        return $city->save();
    }

    public function update($input)
    {
        $city = CitiesModel::find($input['uuid']);
        $city->city = $input['city'];
        $city->city_type = $input['city_type'];
        $city->country = $input['country'];
        $city->about = $input['about'];
        $city->cover_image = $input['cover_image'];

        return $city->save();
    }

    public function delete($id)
    {
        return CitiesModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $city = CitiesModel::uuid($id);

        return [
            'uuid'=>$city->uuid,
            'city'=>$city->city,
            'country'=>$city->country,
            'city_type'=>$city->city_type,
            'about'=>$city->about,
            'cover_image'=>$city->cover_image
        ];
    }

    public function showAll($search = null)
    {
        $cities = CitiesModel::all();
        $data =[];

        foreach ($cities as $city){
            $data[]=[
                'uuid'=>$city->uuid,
                'city'=>$city->city,
                'country'=>$city->country,
                'city_type'=>$city->city_type,
                'about'=>$city->about,
                'cover_image'=>$city->cover_image
            ];
        }

        return $data;
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function isCityExist($city, $uuid = null)
    {
        $query = CitiesModel::query();
        if($uuid != null){
            $query->where('uuid','<>',$uuid);
        }
        $query->where('city','=',$city);

        return ($query->count()>0);
    }

    public function getCityByCityType()
    {
        $internationals = CitiesModel::where('city_type','=','international')->get();
        $domestics = CitiesModel::where('city_type','=','domestic')->get();
        $cityDomestics=[];
        $cityInternationals=[];
        $data=[];

        foreach($internationals as $international){
            $cityInternationals[]=[
                'uuid'=>$international->uuid,
                'city'=>$international->city
            ];
        }

        foreach($domestics as $domestic){
            $cityDomestics[]=[
                'uuid'=>$domestic->uuid,
                'city'=>$domestic->city
            ];
        }

        $data['domestics']=$cityDomestics;
        $data['internationals']=$cityInternationals;

        return $data;
    }

    public function getCity()
    {
        $model = new CitiesModel();
    }


}