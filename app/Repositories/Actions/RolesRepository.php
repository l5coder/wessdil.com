<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/18/19
 * Time: 11:25 AM
 */

namespace App\Repositories\Actions;


use App\Models\RolesModel;
use App\Repositories\Contracts\IRolesRepository;

class RolesRepository implements IRolesRepository
{
    public function create($input)
    {
        $role = new RolesModel();
        $role->role = $input['role'];

        return $role->save();
    }

    public function update($input)
    {
        $role = RolesModel::uuid($input['uuid']);
        $role->role = $input['role'];

        return $role->save();
    }

    public function read($id)
    {
        $role = RolesModel::uuid($id);

        return[
            'uuid'=>$role->uuid,
            'role'=>$role->role,
        ];
    }

    public function delete($id)
    {
        return RolesModel::uuid($id)->delete();
    }

    public function showAll($search = null)
    {
        $data = [];
        $query = RolesModel::query();

        if ($search!= null){
            $query->where('role', 'like', $search.'%');
        }

        $roles = $query->paginate(10);

        foreach ($roles as $role){
            $data[]=[
                'uuid'=>$role->uuid,
                'role'=>$role->role,
            ];
        }

        return $data;
    }

    public function pagination($page = 1, $rows = 10, $sortBy = 'uuid', $order = 'asc', $search = null, $filter = null)
    {
        // TODO: Implement pagination() method.
    }


    public function isRoleNameExist($roleName, $uuid = null)
    {
        $query = RolesModel::query();
        $query->where('role','=',$roleName);
        if ($uuid!= null){
            $query->where('uuid','<>',$uuid);
        }

        return ($query->count()>0);
    }
}