<?php
/**
 * Created by PhpStorm.
 * User: klf
 * Date: 20/06/2019
 * Time: 21:26
 */

namespace App\Repositories\Actions;


use App\Models\CustomersModel;
use App\Repositories\Contracts\ICustomersRepository;

class CustomersRepository implements ICustomersRepository
{
    public function create($input)
    {
        $customer = new CustomersModel();
        $customer->name = $input['name'];
        $customer->address = $input['address'];
        $customer->birth = $input['birth'];
        $customer->citizenship = $input['citizenship'];
        $customer->job = $input['job'];
        $customer->ktp = $input['ktp'];
        $customer->ktp_img = $input['ktp_img'];
        $customer->company_name = $input['company_name'];
        $customer->company_address = $input['company_address'];
        $customer->purchase_purpose = $input['purchase_purpose'];
        $customer->fund_source = $input['fund_source'];
        $customer->privilege = $input['privilege'];
        $customer->status = $input['status'];

        return $customer->save();
    }

    public function read($id)
    {
        $customer = CustomersModel::uuid($id);

        return[
            'name'=>$customer->name,
            'address'=>$customer->address,
            'birth'=>$customer->birth,
            'citizenship'=>$customer->citizenship,
            'job'=>$customer->job,
            'ktp'=>$customer->ktp,
            'ktp_img'=>$customer->ktp_img,
            'company_name'=>$customer->company_name,
            'company_address'=>$customer->company_address,
            'purchase_purpose'=>$customer->purchase_purpose,
            'fund_source'=>$customer->fund_source,
            'privilege'=>$customer->privilege,
            'status'=>$customer->status,
        ];
    }

    public function update($input)
    {
        $customer = CustomersModel::uuid($input['uuid']);
        $customer->name = $input['name'];
        $customer->address = $input['address'];
        $customer->birth = $input['birth'];
        $customer->citizenship = $input['citizenship'];
        $customer->job = $input['job'];
        $customer->ktp = $input['ktp'];
        $customer->ktp_img = $input['ktp_img'];
        $customer->company_name = $input['company_name'];
        $customer->company_address = $input['company_address'];
        $customer->purchase_purpose = $input['purchase_purpose'];
        $customer->fund_source = $input['fund_source'];
        $customer->privilege = $input['privilege'];
        $customer->status = $input['status'];

        return $customer->save();
    }

    public function delete($id)
    {
        return CustomersModel::uuid($id)->delete();
    }

    public function showAll($page = 1, $rows = 10, $sortBy = 'uuid', $order = 'asc',$search = null,$filter = null)
    {
        $data = [];
        $query = CustomersModel::query();

        if($search!= null){
            $query->where( 'user','like',$search.'%');
        }

        $customers = $query->paginate(10);

        foreach ($customers as $customer){
            $data[]=[
                'name'=>$customer->name,
                'address'=>$customer->address,
                'birth'=>$customer->birth,
                'citizenship'=>$customer->citizenship,
                'job'=>$customer->job,
                'ktp'=>$customer->ktp,
                'ktp_img'=>$customer->ktp_img,
                'company_name'=>$customer->company_name,
                'company_address'=>$customer->company_address,
                'purchase_purpose'=>$customer->purchase_purpose,
                'fund_source'=>$customer->fund_source,
                'privilege'=>$customer->privilege,
                'status'=>$customer->status,
            ];
        }
        return $data;
    }

    public function pagination($page = 1, $rows = 10, $sortBy = 'uuid', $order = 'asc', $search = null, $filter = null)
    {
        // TODO: Implement pagination() method.
    }


    public function isCustomerNameExist($customerName, $uuid = null)
    {
        $query = CustomersModel::query();
        $query->where('user','=',$customerName);
        if ($uuid!= null){
            $query->where('uuid', '<>',$uuid);
        }

        return ($query->count()>0);
    }
}