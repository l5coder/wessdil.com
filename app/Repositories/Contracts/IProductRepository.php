<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-07-30
 * Time: 15:15
 */

namespace App\Repositories\Contracts;


interface IProductRepository extends IBaseRepository
{
    public function isSlugExist($slug,$uuid = null);

    public function getProductByDestination($destinationUuid);
}