<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-07-27
 * Time: 13:42
 */

namespace App\Repositories\Contracts;


interface IDestinationRepository extends IBaseRepository
{
    public function isDestinationExist($destinationName,$uuid = null);
}