<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/18/19
 * Time: 11:24 AM
 */

namespace App\Repositories\Contracts;


interface IRolesRepository extends IBaseRepository
{
    public function isRoleNameExist($roleName, $uuid = null);
}