<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 4:11 AM
 */

namespace App\Repositories\Contracts;


interface IRoleUserRepository extends IBaseRepository
{
    public function isRoleUserExist($roleUser, $uuid = null);
}