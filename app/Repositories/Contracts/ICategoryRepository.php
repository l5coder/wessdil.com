<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-07-27
 * Time: 19:34
 */

namespace App\Repositories\Contracts;


interface ICategoryRepository extends IBaseRepository
{
    public function isCategoryExist($categoryName,$uuid = null);

    public function categoryTreeview($parentUuid = null);

    public function isHaveChildren($uuid);

    public function deleteChildren($parentUuid);

    public function getChildren($parentUuid);
}