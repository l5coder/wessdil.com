<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-09-01
 * Time: 11:18
 */

namespace App\Repositories\Contracts;


interface IProductCategoryGroupsRepository extends IBaseRepository
{
    public function deleteCategory($productUuid,$categoryUuid);

    public function deleteProduct($productUuid);
}