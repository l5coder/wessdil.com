<?php
/**
 * Created by PhpStorm.
 * User: klf
 * Date: 20/06/2019
 * Time: 22:03
 */

namespace App\Repositories\Contracts;


interface IVendorPicsRepository extends IBaseRepository
{
    public function isVendorPicNameExist($vendor_picName, $uuid = null);
}