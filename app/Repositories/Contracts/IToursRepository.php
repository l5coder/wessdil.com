<?php

namespace App\Repositories\Contracts;


interface IToursRepository extends IBaseRepository
{
    public function showPagingFrontOffice($sorting,$search = null,$filter = null,$priceRange = null);
}