<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 9:39 PM
 */

namespace App\Repositories\Contracts;


interface IVendorsRepository extends IBaseRepository
{
    public function isVendorNameExist($vendorName, $uuid = null);
}