<?php
/**
 * Created by PhpStorm.
 * User: klf
 * Date: 18/06/2019
 * Time: 13:04
 */

namespace App\Repositories\Contracts;


interface IScopesRepository extends IBaseRepository
{
    public function isScopeNameExist($scopeName, $uuid = null);
}