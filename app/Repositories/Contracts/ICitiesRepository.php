<?php

namespace App\Repositories\Contracts;


interface ICitiesRepository extends IBaseRepository
{
    public function isCityExist($city,$uuid = null);

    public function getCityByCityType();

    public function getCity();
}