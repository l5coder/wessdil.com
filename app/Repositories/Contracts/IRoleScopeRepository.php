<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 3:40 AM
 */

namespace App\Repositories\Contracts;


interface IRoleScopeRepository extends IBaseRepository
{
    public function isRoleScopeExist($roleScope, $uuid = null);
}