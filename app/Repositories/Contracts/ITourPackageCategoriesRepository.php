<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-08
 * Time: 00:41
 */

namespace App\Repositories\Contracts;


interface ITourPackageCategoriesRepository extends IBaseRepository
{
    public function isTourPackageCategoryExist($packageCategory,$uuid = null);
}