<?php
/**
 * Created by PhpStorm.
 * User: klf
 * Date: 20/06/2019
 * Time: 20:35
 */

namespace App\Repositories\Contracts;


interface ICustomersRepository extends IBaseRepository
{
    public function isCustomerNameExist($customerName, $uuid = null);
}