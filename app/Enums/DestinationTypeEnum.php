<?php

namespace App\Enums;

use Nasyrov\Laravel\Enums\Enum;

class DestinationTypeEnum extends Enum
{
    const INTERNATIONAL = 'International';
    const DOMESTIC = 'Domestic';
}
