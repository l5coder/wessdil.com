<?php

namespace App\Enums;

use Nasyrov\Laravel\Enums\Enum;

class VendorTypeEnum extends Enum
{
    // constants
    const TRAVEL_AGENT = 'Travel Agent';
    const TRANSPORTATION = 'Transportation';
    const ATTRACTIONS = 'Attractions';
    const RESTAURANT = 'Restaurant';
    const HOTEL = 'Hotel';
}
