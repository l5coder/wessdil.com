<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 4:16 AM
 */

namespace App\Services;


use App\Services\Response\ServiceResponseDto;

class RoleUserService extends BaseService
{
    protected $roleUserRepository;

    public function __construct(IRoleUserRepository $roleUserRepository)
    {
        $this->roleUserRepository = $roleUserRepository;
    }

    protected function isRoleUserExist($roleUser, $uuid = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->roleUserRepository->isRoleUserExist($roleUser,$uuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $isRoleUserExist = $this->isRoleUserExist($input['role_user']);

        if ($isRoleUserExist->isSuccess()){
            if (!$isRoleUserExist->getResult()){
                try{
                    $this->roleUserRepository->create($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Role User already exist');
            }
        }else{
            $response->addErrorMessage($isRoleUserExist->getErrorMessages());
        }

        return $response;
    }

    public function read($uuid){
        return $this->readObject($this->roleUserRepository,$uuid);
    }

    public function showAll(){
        return $this->getAllObject($this->roleUserRepository);
    }

    public function update($input, $uuid){
        $response = new ServiceResponseDto();
        $isRoleUserExist = $this->isRoleUserExist($input['role_user'],$uuid);

        if ($isRoleUserExist->isSuccess()){
            if (!$isRoleUserExist->getResult()){
                try{
                    $input['uuid']=$uuid;
                    $this->roleUserRepository->update($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Role User already exist');
            }
        }else{
            $response->addErrorMessage($isRoleuserExist->getErrorMessages());
        }

        return $response;
    }

    public function delete($uuid){
        return $this->deleteObject($this->roleUserRepository,$uuid);
    }
}