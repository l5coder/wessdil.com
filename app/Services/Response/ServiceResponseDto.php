<?php 

namespace App\Services\Response;

class ServiceResponseDto {
    
    private $errorMessages =[];
    private $result;
    private $statusCode = 500;

    public function getErrorMessages() {
        return $this->errorMessages;
    }

    public function getResult() {
        return $this->result;
    }

    public function isSuccess() {
        return count($this->errorMessages) == 0;
    }
    
    public function addErrorMessage($message) {
        if(is_array($message)){
            $this->errorMessages = $message;
        }else{
            array_push($this->errorMessages,$message);
        }

    }

    public function setResult($result) {
        $this->result = $result;
    }

    public function setStatusCode($statuCode){
        $this->statusCode = $statuCode;
    }

    public function getStatusCode(){
        return $this->statusCode;
    }

}
