<?php
/**
 * Created by PhpStorm.
 * User: klf
 * Date: 18/06/2019
 * Time: 13:30
 */

namespace App\Services;


use App\Repositories\Contracts\IScopesRepository;
use App\Services\Response\ServiceResponseDto;

class ScopesService extends BaseService
{
    protected $scopesRepository;

    public function __construct(IScopesRepository $scopesRepository)
    {
        $this->scopesRepository = $scopesRepository;
    }

    protected function isScopeNameExist($scopeName, $uuid = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->scopesRepository->isScopeNameExist($scopeName,$uuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $isScopeNameExist = $this->isScopeNameExist($input['scope']);

        if ($isScopeNameExist->isSuccess()){
            if(!$isScopeNameExist->getResult()){
                try{
                    $this->scopesRepository->create($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Scope already exist');
            }
        }else{
            $response->addErrorMessage($isScopeNameExist->getErrorMessage());
        }

        return $response;
    }

    public function read($uuid)
    {
        return $this->readObject($this->scopesRepository, $uuid);
    }

    public function showAll(){
        return $this->getAllObject($this->scopesRepository);
    }

    public function pagination(){
        return $this->getPaginationData($this->scopesRepository);
    }

    public function update($input, $uuid){
        $response = new ServiceResponseDto();
        $isScopeNameExist = $this->isScopeNameExist($input['role'], $uuid);

        if($isScopeNameExist->isSuccess()){
            if (!$isScopeNameExist->getResult()){
                try{
                    $input['uuid']=$uuid;
                    $this->scopesRepository->update($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Scope already exist');
            }
        }else{
            $response->addErrorMessage($isScopeNameExist->getErrorMessage());
        }

        return $response;

    }


    public function delete($uuid){
        return $this->deleteObject($this->scopesRepository,$uuid);
    }

}