<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-08
 * Time: 00:48
 */

namespace App\Services;


use App\Repositories\Contracts\ITourPackageCategoriesRepository;
use App\Services\Response\ServiceResponseDto;

class TourPackageCategoriesService extends BaseService
{
    protected $tourPackageCategoriesRepository;

    public function __construct(ITourPackageCategoriesRepository $tourPackageCategoriesRepository)
    {
        $this->tourPackageCategoriesRepository = $tourPackageCategoriesRepository;
    }

    protected function isPackageCategoryExist($packageCategory,$uuid = null){
        $response = new ServiceResponseDto();

        $response->setResult($this->tourPackageCategoriesRepository->isTourPackageCategoryExist($packageCategory,$uuid));

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $isPackageCategoryExist = $this->isPackageCategoryExist($input['package_category'])->getResult();

        if($isPackageCategoryExist){
            $message = 'Package category already exist';
            $response->addErrorMessage($message);
        }else{
            try{
                $this->tourPackageCategoriesRepository->create($input);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    public function read($id){
        return $this->readObject($this->tourPackageCategoriesRepository,$id);
    }

    public function showAll(){
        return $this->getAllObject($this->tourPackageCategoriesRepository);
    }

    public function update($input,$uuid){
        $response = new ServiceResponseDto();
        $isPackageCategoryExist = $this->isPackageCategoryExist($input['package_category'],$uuid)->getResult();

        if($isPackageCategoryExist){
            $message = 'Package category already exist';
            $response->addErrorMessage($message);
        }else{
            try{
                $input['uuid'] = $uuid;
                $this->tourPackageCategoriesRepository->update($input);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    public function delete($id){
        return $this->deleteObject($this->tourPackageCategoriesRepository,$id);
    }
}