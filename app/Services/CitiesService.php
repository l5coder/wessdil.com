<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-10
 * Time: 15:27
 */

namespace App\Services;


use App\Repositories\Contracts\ICitiesRepository;
use App\Services\Response\ServiceResponseDto;
use Illuminate\Support\Facades\Storage;

class CitiesService extends BaseService
{
    protected $citiesRepository;

    public function __construct(ICitiesRepository $citiesRepository)
    {
        $this->citiesRepository = $citiesRepository;
    }

    protected function isCityExist($city,$uuid = null){
        $response = new ServiceResponseDto();

        $response->setResult($this->citiesRepository->isCityExist($city,$uuid));

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $isCityExist = $this->isCityExist($input['city'])->getResult();

        if($isCityExist){
            $message = 'City already exist';
            $response->addErrorMessage($message);
        }else{
            try{
                $this->citiesRepository->create($input);
            }catch (\Exception $exception){
                $message = $exception->getMessage();
                $response->addErrorMessage($message);
            }
        }

        return $response;
    }

    public function read($id){
        return $this->readObject($this->citiesRepository,$id);
    }

    public function showAll(){
        return $this->getAllObject($this->citiesRepository);
    }

    public function update($input,$uuid){
        $response = new ServiceResponseDto();
        $isCityExist = $this->isCityExist($input['city'],$uuid)->getResult();

        if($isCityExist){
            $message = 'City already exist';
            $response->addErrorMessage($message);
        }else{
            try{
                $this->citiesRepository->update($input);
            }catch (\Exception $exception){
                $message = $exception->getMessage();
                $response->addErrorMessage($message);
            }
        }

        return $response;
    }

    public function delete($uuid){
        return $this->deleteObject($this->citiesRepository,$uuid);
    }

    public function getCitiesByCityType(){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->citiesRepository->getCityByCityType());
        }catch (\Exception $exception){
            $message = $exception->getMessage();
            $response->addErrorMessage($message);
        }

        return $response;
    }

    public function getCountry(){
        $response = new ServiceResponseDto();
        $data = [];

        $countryJson = Storage::get('/countries.json');
        $countries = json_decode($countryJson);

        foreach ($countries as $country){
            $data[]=[
                'id'=>$country->name,
                'text'=>$country->name
            ];
        }

        $response->setResult($data);

        return $response;

    }

    public function FunctionName()
    {
        $response = new ServiceResponseDto();

        $response->setResult($this->citiesRepository->getCity());
    }
}