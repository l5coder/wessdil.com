<?php

namespace App\Services;


use App\Services\Response\ServiceResponseDto;

class UploadHandlerService extends BaseService
{

    public function uploadHandlerImage($file,$directory,$thumbDirectory){
        $response = new ServiceResponseDto();

        $uploadAttachment = $this->uploadAttachment($file,'upload');

        if($uploadAttachment->isSuccess()){
            try{
                $url= 'storage/'.$uploadAttachment->getResult();
                $response->setResult($url);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }else{
            $response->addErrorMessage($uploadAttachment->getErrorMessages());
        }

        return $response;
    }

    public function uploadHandlerFile($file,$directory){
        return $this->uploadHandlerFile($file,$directory);
    }
}