<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-07-27
 * Time: 19:40
 */

namespace App\Services;


use App\Repositories\Contracts\ICategoryRepository;
use App\Services\Response\ServiceResponseDto;

class CategoryService extends BaseService
{
    protected $categoryRepository;

    public function __construct(ICategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    protected function isCategoryExist($categoryName,$uuid = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->categoryRepository->isCategoryExist($categoryName,$uuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $getIsCategoryExist = $this->isCategoryExist($input['name']);

        if($getIsCategoryExist->isSuccess()){
            $isCategoryExist = $getIsCategoryExist->getResult();
            if(!$isCategoryExist){
                try{
                    $this->categoryRepository->create($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Category already exist');
            }
        }else{
            $response->addErrorMessage($getIsCategoryExist->getErrorMessages());
        }

        return $response;
    }

    public function read($uuid){
        return $this->readObject($this->categoryRepository,$uuid);
    }

    public function showAll(){
        return $this->getAllObject($this->categoryRepository);
    }

    public function update($input,$uuid){
        $response = new ServiceResponseDto();
        $getIsCategoryExist = $this->isCategoryExist($input['name'],$uuid);

        if($getIsCategoryExist->isSuccess()){
            $isCategoryExist = $getIsCategoryExist->getResult();
            if(!$isCategoryExist){
                try{
                    $input['uuid'] = $uuid;
                    $this->categoryRepository->update($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Category already exist');
            }
        }else{
            $response->addErrorMessage($getIsCategoryExist->getErrorMessages());
        }

        return $response;
    }

    protected function deleteChildren($parentUuid){
        $response = new ServiceResponseDto();

        try{
            $this->categoryRepository->deleteChildren($parentUuid);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function delete($uuid){
        $response = new ServiceResponseDto();

        try{
            $isHaveChildren = $this->categoryRepository->isHaveChildren($uuid);
            if($isHaveChildren){
                $deleteChildren = $this->deleteChildren($uuid);
                if($deleteChildren->isSuccess()){
                   $this->categoryRepository->delete($uuid);
                }else{
                    $response->addErrorMessage($deleteChildren->getErrorMessages());
                }
            }else{
                $this->categoryRepository->delete($uuid);
            }
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getCategoryOptions(){
        $response = new ServiceResponseDto();
        $data = [];
        try{
            $categories = $this->categoryRepository->showAll();
            foreach ($categories as $category){
                $data[]=[
                    'value'=>$category['uuid'],
                    'text'=>$category['name']
                ];
            }
            $response->setResult($data);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function categoryTreeview(){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->categoryRepository->categoryTreeview());
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }
}