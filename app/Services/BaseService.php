<?php

namespace App\Services;

use App\Repositories\Contracts\IBaseRepository;
use App\Repositories\Contracts\Pagination\PaginationParam;
use App\Services\Response\ServiceResponseDto as ResponseDto;
use App\Services\Response\ServicePaginationResponseDto as PaginationDto;
use App\Services\Response\ServiceResponseDto;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

abstract class BaseService {
    protected function generateDefaultPassword($length = 8){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    protected function generateSlug($url){
        $url = strtolower($url);
        //Make alphanumeric (removes all other characters)
        $url = preg_replace("/[^a-z0-9_\s-]/", "", $url);
        //Clean up multiple dashes or whitespaces
        $url = preg_replace("/[\s-]+/", " ", $url);
        //Convert whitespaces and underscore to dash
        $url = preg_replace("/[\s_]/", "-", $url);
        return $url;
    }

    protected function deleteObject(IBaseRepository $repository, $id) {
        $response = new ResponseDto();

        $response->setResult($repository->delete($id));

        return $response;
    }

    protected function readObject(IBaseRepository $repository, $id) {
        $response = new ResponseDto();
        $response->setResult($repository->read($id));

        return $response;
    }

    protected function getAllObject(IBaseRepository $repository) {
        $response = new ResponseDto();
        try{
            $response->setResult($repository->showAll());
        }catch (\Exception $exception){
            $message = $exception->getMessage();
            $response->addErrorMessage($message);
        }

        return $response;
    }

    protected function getPaginationObject(IBaseRepository $repository, $param) {
        $response = new PaginationDto();
   
        $pagingResult = $repository->paginationData($this->parsePaginationParam($param));
        $response->setCurrentPage($param['pageIndex']);
        $response->setPageSize($param['pageSize']);
        $response->setTotalCount($pagingResult->getTotalCount());
        $response->setResult($pagingResult->getResult());

        return $response;
    }

    protected function parsePaginationParam($param) {
        $paginationParam = new PaginationParam();
        $paginationParam->setKeyword($param['searchPhrase']);
        $paginationParam->setPageIndex($param['pageIndex']);
        $paginationParam->setPageSize($param['pageSize']);

        if ($param['sort'] != '') {
            foreach ($param['sort'] as $key => $value) {
                $paginationParam->setSortBy($key);
                $paginationParam->setSortOrder($value);
            }
        }
        
        return $paginationParam;
    }
    protected function uploadingImage($file,$directory = 'public',$thumbDirectory = 'public/thumb',$width = 150, $height = 150){
        $response = new ServiceResponseDto();

        try{
            $file->storeAs($directory,$file->getClientOriginalName());
            $image = Image::make($file);
            $image->resize($width,$height);
            Storage::put($thumbDirectory.'/'.$file->getClientOriginalName(),(string)$image->encode());
            $response->setResult($file->getClientOriginalName());

        }catch (\Exception $exception){
            $message = $exception->getMessage();
            $response->addErrorMessage($message);
        }

        return $response;
    }

    protected function uploadingFile($file,$directory){
        $response = new ServiceResponseDto();

        try{
            $file->storeAs($directory,$file->getClientOriginalName());
            $response->setResult($file->getClientOriginalName());

        }catch (\Exception $exception){
            $message = $exception->getMessage();
            $response->addErrorMessage($message);
        }

        return $response;
    }

    public function uploadAttachment($file,$directory = '/upload'){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($file->store($directory,'public'));
        }catch (\Exception $exception){
            $message = $exception->getMessage();
            $response->addErrorMessage($message);
        }

        return $response;
    }

}
