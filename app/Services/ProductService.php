<?php

namespace App\Services;


use App\Repositories\Contracts\ICategoryRepository;
use App\Repositories\Contracts\IProductCategoryGroupsRepository;
use App\Repositories\Contracts\IProductRepository;
use App\Services\Response\ServiceResponseDto;

class ProductService extends BaseService
{
    protected $productRepository;
    protected $categoryRepository;
    protected $productCategoryGroupRepository;

    public function __construct(IProductRepository $productRepository,
                                ICategoryRepository $categoryRepository,
                                IProductCategoryGroupsRepository $productCategoryGroupRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->productCategoryGroupRepository = $productCategoryGroupRepository;
    }

    protected function isSlugExist($slug,$uuid = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->productRepository->isSlugExist($slug,$uuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function isCategoriesHaveChildren($categoryUuid){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->categoryRepository->isHaveChildren($categoryUuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function getCategoryChildren($categoryUuid){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->categoryRepository->getChildren($categoryUuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }
        return $response;
    }

    protected function createProductCategoryGroups($categoryUuids, $productUuid){
        $response = new ServiceResponseDto();
        try{
           for($i=0;$i<count($categoryUuids);$i++){
               $isCategoryHaveChildren = $this->isCategoriesHaveChildren($categoryUuids[$i]);
               if($isCategoryHaveChildren->getResult()){
                   $getChildrens = $this->getCategoryChildren($categoryUuids[$i]);
                   $childrens =$getChildrens->getResult();
                   array_push($categoryUuids,$childrens);
               }
           }

            for($i=0;$i<count($categoryUuids);$i++){
                $input = [
                    'productUuid'=>$productUuid,
                    'categoryUuid'=>$categoryUuids[$i]
                ];
                if(!$this->productCategoryGroupRepository->create($input)){
                    $response->addErrorMessage('Failed save product category group');
                }
            }
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $slug = $this->generateSlug($input['title']);
        $getIsSlugExist = $this->isSlugExist($slug);

        if($getIsSlugExist->isSuccess()){
            $isSlugExist = $getIsSlugExist->getResult();
            if(!$isSlugExist){
                try{
                    $input['slug'] = $slug;

                    if($productUuid = $this->productRepository->create($input)){
                        $createCategoryProduct = $this->createProductCategoryGroups($input['category_uuid'],$productUuid);
                        if(!$createCategoryProduct->isSuccess()){
                            $response->addErrorMessage($createCategoryProduct->getErrorMessages());
                        }
                    }else{
                        $response->addErrorMessage('Error : failed to save product');
                    }
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Cannot save product your product title is similiar with other products');
            }
        }else{
            $response->addErrorMessage($getIsSlugExist->getErrorMessages());
        }

        return $response;
    }

    public function read($uuid){
        return $this->readObject($this->productRepository,$uuid);
    }

    public function showAll(){
        return $this->getAllObject($this->productRepository);
    }

    public function update($input,$uuid){
        $response = new ServiceResponseDto();
        $slug = $this->generateSlug($input['title']);
        $getIsSlugExist = $this->isSlugExist($slug,$uuid);

        if($getIsSlugExist->isSuccess()){
            $isSlugExist = $getIsSlugExist->getResult();
            if(!$isSlugExist){
                try{
                    $input['slug'] = $slug;
                    $input['uuid'] = $uuid;
                    $this->productRepository->update($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Cannot save product your product title is similiar with other products, you must make unique title');
            }
        }else{
            $response->addErrorMessage($getIsSlugExist->getErrorMessages());
        }

        return $response;
    }

    public function delete($uuid){
        return $this->deleteObject($this->productRepository,$uuid);
    }

    public function getProductByDestination($destinationUuid){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->productRepository->getProductByDestination($destinationUuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }
}