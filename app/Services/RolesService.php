<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/18/19
 * Time: 11:50 AM
 */

namespace App\Services;


use App\Repositories\Contracts\IRolesRepository;
use App\Services\Response\ServiceResponseDto;

class RolesService extends BaseService
{
    protected $rolesRepository;

    public function __construct(IRolesRepository $rolesRepository)
    {
        $this->rolesRepository = $rolesRepository;
    }

    protected function isRoleNameExist($roleName, $uuid = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->rolesRepository->isRoleNameExist($roleName,$uuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $isRoleNameExist = $this->isRoleNameExist($input['role']);

        if ($isRoleNameExist->isSuccess()){
            if (!$isRoleNameExist->getResult()){
                try{
                    $this->rolesRepository->create($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Role already exist');
            }
        }else{
            $response->addErrorMessage($isRoleNameExist->getErrorMessages());
        }

        return $response;
    }

    public function read($uuid){
        return $this->readObject($this->rolesRepository,$uuid);
    }

    public function showAll(){
        return $this->getAllObject($this->rolesRepository);
    }

    public function update($input, $uuid){
        $response = new ServiceResponseDto();
        $isRoleNameExist = $this->isRoleNameExist($input['role'],$uuid);

        if ($isRoleNameExist->isSuccess()){
            if (!$isRoleNameExist->getResult()){
                try{
                    $input['uuid']=$uuid;
                    $this->rolesRepository->update($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Role already exist');
            }
        }else{
            $response->addErrorMessage($isRoleNameExist->getErrorMessages());
        }

        return $response;
    }

    public function delete($uuid){
        return $this->deleteObject($this->rolesRepository,$uuid);
    }
}