<?php

namespace App\Services;


use App\Repositories\Contracts\IToursRepository;
use App\Services\Response\ServiceResponseDto;

class ToursService extends BaseService
{
    protected $oneDayToursRepository;

    public function __construct(IToursRepository $oneDayToursRepository)
    {
        $this->oneDayToursRepository = $oneDayToursRepository;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $input['slug'] = $this->generateSlug($input['title']);

        try{
            $this->oneDayToursRepository->create($input);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function read($id){
        return $this->readObject($this->oneDayToursRepository,$id);
    }

    public function showAll(){
        return $this->getAllObject($this->oneDayToursRepository);
    }

    public function update($input,$uuid){
        $response = new ServiceResponseDto();
        $input['slug'] = $this->generateSlug($input['title']);
        $input['uuid'] = $uuid;

        try{
            $this->oneDayToursRepository->update($input);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function delete($id){
        return $this->deleteObject($this->oneDayToursRepository,$id);
    }

    public function showPagingFrontOffice($sorting,$search = null, $filter = null, $priceRange = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->oneDayToursRepository->showPagingFrontOffice($sorting,$search,$filter,$priceRange));
        }catch (\Exception $exception){
            $message = $exception->getMessage();
            $response->addErrorMessage($message);
        }

        return $response;
    }

    public function uploadCoverImage($file){
        $directory = 'public/tour-thumb';
        $width = 300;
        $height = 300;

        return $this->uploadingFile($file,$directory,$width,$height);
    }

    public function tinyMceUploadHandler($file){
        $directory = 'public/tour-thumb';

        return $this->uploadingFile($file,$directory);
    }

}