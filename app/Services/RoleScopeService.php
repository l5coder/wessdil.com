<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 3:53 AM
 */

namespace App\Services;


use App\Repositories\Contracts\IRoleScopeRepository;
use App\Services\Response\ServiceResponseDto;

class RoleScopeService extends BaseService
{
    protected $roleScopeRepository;

    public function __construct(IRoleScopeRepository $roleScopeRepository)
    {
        $this->roleScopeRepository = $roleScopeRepository;
    }

    protected function isRoleScopeExist($roleScope, $uuid = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->roleScopeRepository->isRoleScopeExist($roleScope,$uuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $isRoleScopeExist = $this->isRoleScopeExist($input['role_scope']);

        if ($isRoleScopeExist->isSuccess()){
            if (!$isRoleScopeExist->getResult()){
                try{
                    $this->roleScopeRepository->create($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Role Scope already exist');
            }
        }else{
            $response->addErrorMessage($isRoleScopeExist->getErrorMessages());
        }

        return $response;
    }

    public function read($uuid){
        return $this->readObject($this->roleScopeRepository,$uuid);
    }

    public function showAll(){
        return $this->getAllObject($this->roleScopeRepository);
    }

    public function update($input, $uuid){
        $response = new ServiceResponseDto();
        $isRoleScopeExist = $this->isRoleScopeExist($input['role_scope'],$uuid);

        if ($isRoleScopeExist->isSuccess()){
            if (!$isRoleScopeExist->getResult()){
                try{
                    $input['uuid']=$uuid;
                    $this->roleScopeRepository->update($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Role Scope already exist');
            }
        }else{
            $response->addErrorMessage($isRoleScopeExist->getErrorMessages());
        }

        return $response;
    }

    public function delete($uuid){
        return $this->deleteObject($this->roleScopeRepository,$uuid);
    }
}