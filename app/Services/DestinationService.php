<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-07-27
 * Time: 13:49
 */

namespace App\Services;


use App\Enums\DestinationTypeEnum;
use App\Repositories\Contracts\IDestinationRepository;
use App\Services\Response\ServiceResponseDto;

class DestinationService extends BaseService
{
    protected $destinationRepository;

    public function __construct(IDestinationRepository $destinationRepository)
    {
        $this->destinationRepository = $destinationRepository;
    }

    protected function isDestinationExist($destinationName,$uuid = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->destinationRepository->isDestinationExist($destinationName,$uuid));
        }catch (\Exception $exception){
            $response->addErrorMessage('tes'.$exception->getMessage());
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $getIsDestinationExist = $this->isDestinationExist($input['name']);

        if($getIsDestinationExist->isSuccess()){
            $isDestinationExist = $getIsDestinationExist->getResult();
            if(!$isDestinationExist){
                try{
                    $slug = $this->generateSlug($input['name']);
                    $input['slug'] = $slug;
                    $this->destinationRepository->create($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Destination already exist');
            }
        }

        return $response;
    }

    public function read($uuid){
        return $this->readObject($this->destinationRepository,$uuid);
    }

    public function showAll(){
        return $this->getAllObject($this->destinationRepository);
    }

    public function update($input,$uuid){
        $response = new ServiceResponseDto();
        $getIsDestinationExist = $this->isDestinationExist($input['name'],$uuid);

        if($getIsDestinationExist->isSuccess()){
            $isDestinationExist = $getIsDestinationExist->getResult();
            if(!$isDestinationExist){
                try{
                    $input['uuid'] = $uuid;
                    $slug = $input['name'];
                    $input['slug'] = $slug;
                    $this->destinationRepository->update($input);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Destination already exist');
            }
        }

        return $response;
    }

    public function delete($uuid){
        return $this->deleteObject($this->destinationRepository,$uuid);
    }

    public function getDestinationType(){
        $response = new ServiceResponseDto();
        try{
            $data =[];
            $enums = DestinationTypeEnum::values();
            foreach ($enums as $enum => $value){
                array_push($data,$value);
            }
            $response->setResult($data);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getDestinationOptions(){
        $response = new ServiceResponseDto();
        $data = [];
        try{
            $destinations = $this->destinationRepository->showAll();
            foreach ($destinations as $destination){
                $data[]=[
                    'value'=>$destination['uuid'],
                    'text'=>$destination['name']
                ];
            }
            $response->setResult($data);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

}