<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/22/19
 * Time: 1:26 AM
 */

namespace App\Services;


use App\Enums\VendorTypeEnum;
use App\Models\UsersModel;
use App\Repositories\Contracts\IUserRepository;
use App\Repositories\Contracts\IVendorPicsRepository;
use App\Repositories\Contracts\IVendorsRepository;
use App\Services\Response\ServiceResponseDto;

class VendorsService extends BaseService
{
    protected $vendorsRepository;
    protected $vendorPicsRepository;
    protected $userRepository;

    public function __construct(IVendorsRepository $vendorsRepository, IVendorPicsRepository $vendorPicsRepository,IUserRepository $userRepository)
    {
        $this->vendorsRepository = $vendorsRepository;
        $this->vendorPicsRepository = $vendorPicsRepository;
        $this->userRepository = $userRepository;
    }

    protected function isVendorExist($vendorName,$uuid = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->vendorsRepository->isVendorNameExist($vendorName,$uuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function isUserExist($email,$uuid = null){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->userRepository->isUserNameExist($email,$uuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function userCreate($input){
        $response = new ServiceResponseDto();
        $getIsUserExist = $this->isUserExist($input['email']);

        if($getIsUserExist->isSuccess()){
            $isUserExist = $getIsUserExist->getResult();
            if(!$isUserExist){
                try{
                    $password = $this->generateDefaultPassword();
                    $input['password'] = $password;
                    $response->setResult($this->userRepository->create($input));
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Email already registered');
            }
        }else{
            $response->addErrorMessage($getIsUserExist->getErrorMessages());
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $createUser = $this->userCreate($input);

        if($createUser->isSuccess()){
            $userUuid = $createUser->getResult();
            try{
                $input['user_uuid'] = $userUuid;
                $this->vendorsRepository->create($input);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }else{
            $response->addErrorMessage($createUser->getErrorMessages());
        }

        return $response;
    }

    public function read($uuid){
        return $this->readObject($this->vendorsRepository,$uuid);
    }

    public function showAll(){
        return $this->getAllObject($this->vendorsRepository);
    }

    protected function updateUserEmail($uuid,$email){
        $response = new ServiceResponseDto();
        $getIsUserExist = $this->isUserExist($email,$uuid);

        if($getIsUserExist->isSuccess()){
            $isUserExist = $getIsUserExist->getResult();
            if(!$isUserExist){
                try{
                    $this->userRepository->changeUserEmail($uuid,$email);
                }catch (\Exception $exception){
                    $response->addErrorMessage($exception->getMessage());
                }
            }else{
                $response->addErrorMessage('Email already registered');
            }
        }else{
            $response->addErrorMessage($getIsUserExist->getErrorMessages());
        }

        return $response;
    }

    public function update($input,$uuid){
        $response = new ServiceResponseDto();
        $updateUserEmail = $this->updateUserEmail($input['user_uuid'],$input['email']);

        if($updateUserEmail->isSuccess()){
            try{
                $input['uuid']=$uuid;
                $this->vendorsRepository->update($input);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }else{
            $response->addErrorMessage($updateUserEmail->getErrorMessages());
        }

        return $response;
    }
    protected function userDelete($uuid){
        $response = new ServiceResponseDto();
        try{
            $this->userRepository->delete($uuid);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function delete($uuid){
        $response = new ServiceResponseDto();
        $getUserVendor = $this->read($uuid);

        if($getUserVendor->isSuccess()) {
            try{
                $user = $getUserVendor->getResult();
                $userUuid = $user['user_uuid'];

                $deleteUser = $this->userDelete($userUuid);
                if($deleteUser->isSuccess()){
                    $this->vendorsRepository->delete($uuid);
                }
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }else{
            $response->addErrorMessage($getUserVendor->getErrorMessages());
        }

        return $response;
    }

    public function getVendorType(){
        $response = new ServiceResponseDto();
        $data = [];
        try{
            $enums = VendorTypeEnum::values();
            foreach ($enums as $enum => $value){
                array_push($data,$value);
            }
            $response->setResult($data);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getVendorOptions(){
        $response = new ServiceResponseDto();
        $data = [];

        try{
            $vendors = $this->vendorsRepository->showAll();
            foreach ($vendors as $vendor){
                $data[]=[
                    'value'=>$vendor['uuid'],
                    'text'=>$vendor['name']
                ];
            }
            $response->setResult($data);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }
}