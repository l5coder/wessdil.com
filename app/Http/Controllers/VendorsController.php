<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/22/19
 * Time: 1:45 AM
 */

namespace App\Http\Controllers;


use App\Services\VendorsService;
use Illuminate\Support\Facades\Input;

class VendorsController extends Controller
{
    protected $vendorsService;

    public function __construct(VendorsService $vendorsService)
    {
        $this->vendorsService = $vendorsService;
    }

    public function create(){
        $result = $this->vendorsService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->vendorsService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->vendorsService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->vendorsService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result= $this->vendorsService->delete($uuid);

        return $this->getJsonResponse($result);
    }

    public function getVendorType(){
        $result = $this->vendorsService->getVendorType();

        return $this->getJsonResponse($result);
    }

    public function getVendorOptions(){
        $result = $this->vendorsService->getVendorOptions();

        return $this->getJsonResponse($result);
    }
}