<?php

namespace App\Http\Controllers;

use App\Services\UploadHandlerService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UploadHandlerController extends Controller
{
    protected $uploadHandlerService;

    public function __construct(UploadHandlerService $uploadHandlerService)
    {
        $this->uploadHandlerService = $uploadHandlerService;
    }

    public function uploadHandlerImage(){
        $result = $this->uploadHandlerService->uploadHandlerImage(Input::file('file'),Input::get('directory'),Input::get('thumbDirectory'));

        return $this->getJsonResponse($result);
    }

    public function uploadHandlerFile(){
        $result = $this->uploadHandlerService->uploadHandlerFile(Input::file('file'),Input::get('directory'));

        return $this->getJsonResponse($result);
    }
}
