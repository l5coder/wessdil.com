<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function create(){
        $result = $this->productService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->productService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->productService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->productService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->productService->delete($uuid);

        return $this->getJsonResponse($result);
    }

    public function getProductByDestination($destinationUuid){
        $result = $this->productService->getProductByDestination($destinationUuid);

        return $this->getJsonResponse($result);
    }
}
