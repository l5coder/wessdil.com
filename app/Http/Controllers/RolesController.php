<?php

namespace App\Http\Controllers;

use App\Services\RolesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class RolesController extends Controller
{
    protected $rolesService;

    public function __construct(RolesService $rolesService)
    {
        $this->rolesService = $rolesService;
    }

    public function create(){
        $result = $this->rolesService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->rolesService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->rolesService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->rolesService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->rolesService->delete($uuid);

        return $this->getJsonResponse($result);
    }
}
