<?php

namespace App\Http\Controllers;

use App\Services\CitiesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CitiesController extends Controller
{
    protected $citiesService;

    public function __construct(CitiesService $citiesService)
    {
        $this->citiesService = $citiesService;
    }

    public function create(){
        $result = $this->citiesService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->citiesService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->citiesService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->citiesService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->citiesService->delete($uuid);

        return $this->getJsonResponse($result);
    }

    public function getCityByCityType(){
        $result = $this->citiesService->getCitiesByCityType();

        return $this->getJsonResponse($result);
    }

    public function getCountry(){
        $result = $this->citiesService->getCountry();

        return $this->getJsonResponse($result);
    }
}
