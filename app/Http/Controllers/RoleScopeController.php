<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 4:01 AM
 */

namespace App\Http\Controllers;


use App\Services\RoleScopeService;

class RoleScopeController extends Controller
{
    protected $roleScopeService;

    public function __construct(RoleScopeService $roleScopeService)
    {
        $this->roleScopeService = $roleScopeService;
    }

    public function create(){
        $result = $this->roleScopeService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->roleScopeService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->roleScopeService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->roleScopeService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->roleScopeService->delete($uuid);

        return $this->getJsonResponse($result);
    }
}