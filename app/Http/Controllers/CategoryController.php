<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function create(){
        $result = $this->categoryService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->categoryService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->categoryService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->categoryService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->categoryService->delete($uuid);

        return $this->getJsonResponse($result);
    }

    public function getCategoryOptions(){
        $result = $this->categoryService->getCategoryOptions();

        return $this->getJsonResponse($result);
    }

    public function categoryTreeview(){
        $result = $this->categoryService->categoryTreeview();

        return $this->getJsonResponse($result);
    }
}
