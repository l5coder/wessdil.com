<?php

namespace App\Http\Controllers;

use App\Services\ToursService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ToursController extends Controller
{
    protected $toursService;

    public function __construct(ToursService $toursService)
    {
        $this->toursService = $toursService;
    }

    public function create(){
        $result = $this->toursService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($id){
        $result = $this->toursService->read($id);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->toursService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->toursService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->toursService->delete($uuid);

        return $this->getJsonResponse($result);
    }

    public function showPagingFrontOffice(){
        $result = $this->toursService->showPagingFrontOffice(Input::get('s'),Input::get('q'),Input::get('f'),Input::get('pr'));

        return $this->getJsonResponse($result);
    }

    public function uploadCoverImage(){
        $result = $this->toursService->uploadCoverImage(Input::file('file'));

        return $this->getJsonResponse($result);
    }

    public function tinyMceUploadHandler(){
        $result = $this->toursService->tinyMceUploadHandler(Input::file('file'));

        return $this->getJsonResponse($result);
    }
}
