<?php

namespace App\Http\Controllers;

use App\Services\ScopesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ScopesController extends Controller
{
    protected $scopeService;

    public function __construct(ScopesService $scopeService)
    {
        $this->scopeService = $scopeService;
    }

    public function create(){
        $result = $this->scopeService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->scopeService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->scopeService->showAll();

        return $this->getJsonResponse($result);
    }

    public function pagination(){
        $result = $this->scopeService->pagination();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->scopeService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->scopeService->delete($uuid);

        return $this->getJsonResponse($result);
    }
}
