<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 4:28 AM
 */

namespace App\Http\Controllers;


use App\Services\RoleUserService;

class RoleUserController extends Controller
{
    protected $roleUserService;

    public function __construct(RoleUserService $roleUserService)
    {
        $this->roleUserService = $roleUserService;
    }

    public function create(){
        $result = $this->roleUserService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->roleUserService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->roleUserService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->roleUserService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->roleUserService->delete($uuid);

        return $this->getJsonResponse($result);
    }
}