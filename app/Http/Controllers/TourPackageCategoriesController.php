<?php

namespace App\Http\Controllers;

use App\Services\TourPackageCategoriesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TourPackageCategoriesController extends Controller
{
    protected $tourPackageCategoriesService;

    public function __construct(TourPackageCategoriesService $tourPackageCategoriesService)
    {
        $this->tourPackageCategoriesService = $tourPackageCategoriesService;
    }

    public function create(){
        $result = $this->tourPackageCategoriesService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($id){
        $result = $this->tourPackageCategoriesService->read($id);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->tourPackageCategoriesService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->tourPackageCategoriesService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->tourPackageCategoriesService->delete($uuid);

        return $this->getJsonResponse($result);
    }
}
