<?php

namespace App\Http\Controllers;

use App\Services\DestinationService;
use App\Services\Response\ServiceResponseDto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class DestinationController extends Controller
{
    protected $destinationService;

    public function __construct(DestinationService $destinationService)
    {
        $this->destinationService = $destinationService;
    }

    public function create(){
        $result = $this->destinationService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->destinationService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->destinationService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->destinationService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->destinationService->delete($uuid);

        return $this->getJsonResponse($result);
    }

    public function getDestinationType(){
        $result = $this->destinationService->getDestinationType();

        return $this->getJsonResponse($result);
    }

    public function getDestinationOptions(){
        $result = $this->destinationService->getDestinationOptions();

        return $this->getJsonResponse($result);
    }
}
