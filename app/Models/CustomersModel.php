<?php
/**
 * Created by PhpStorm.
 * User: klf
 * Date: 20/06/2019
 * Time: 20:31
 */

namespace App\Models;


use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class CustomersModel extends Model
{
    use Uuids;

    protected $table ='customers';
    protected $primaryKey='uuid';
    public $incrementing=false;
    public $timestamps=false;
}