<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class CitiesModel extends Model
{
    use Uuids;

    protected $table='cities';
    public $timestamps = false;
    protected $primaryKey = 'uuid';
    public $incrementing = false;

}
