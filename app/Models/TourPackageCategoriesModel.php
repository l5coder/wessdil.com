<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class TourPackageCategoriesModel extends Model
{
    use Uuids;

    protected $table= 'tour_package_categories';
    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $fillable = ['package_category'];
}
