<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 9:38 PM
 */

namespace App\Models;


use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class VendorsModel extends Model
{
    use Uuids;

    protected $table='vendors';
    protected $primaryKey='uuid';
    public $incrementing=false;
    public $timestamps=false;
}