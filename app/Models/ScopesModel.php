<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class ScopesModel extends Model
{
    use Uuids;

    protected $table='scopes';
    protected $primaryKey='uuid';
    public $incrementing=false;
    public $timestamps=false;
}
