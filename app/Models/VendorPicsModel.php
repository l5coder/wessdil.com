<?php
/**
 * Created by PhpStorm.
 * User: klf
 * Date: 20/06/2019
 * Time: 22:01
 */

namespace App\Models;


use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class VendorPicsModel extends Model
{
    use Uuids;

    protected $table ='vendor_pics';
    protected $primaryKey='uuid';
    public $incrementing=false;
    public $timestamps=false;
}