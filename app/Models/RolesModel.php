<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class RolesModel extends Model
{
    use Uuids;

    protected $table='roles';
    protected $primaryKey='uuid';
    public $incrementing=false;
    public $timestamps=false;
}
