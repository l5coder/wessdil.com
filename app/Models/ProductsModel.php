<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class ProductsModel extends Model
{
    use Uuids;
    
    protected $table = 'products';
    public $incrementing = false;
    protected $primaryKey = 'uuid';
    protected $casts = [
        'created_at'=>'datetime:d-m-Y',
        'updated_at'=>'datetime:d-m-Y',
        'gallery'=>'array',
        'include'=>'array',
        'exclude'=>'array',
        'facilities'=>'array',
        'itinerary'=>'array',
        'highlights'=>'array',
        'price'=>'array'
    ];
}
