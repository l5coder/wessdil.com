<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class ToursModel extends Model
{
    use Uuids;

    protected $table='tours';
    public $incrementing = false;
    protected $primaryKey = 'uuid';
}
