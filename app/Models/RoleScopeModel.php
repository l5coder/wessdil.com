<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 3:43 AM
 */

namespace App\Models;


use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class RoleScopeModel extends Model
{
    use Uuids;

    protected $table='role_scope';
    protected $primaryKey='uuid';
    public $incrementing=false;
    public $timestamps=false;
}