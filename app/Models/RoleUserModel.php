<?php
/**
 * Created by PhpStorm.
 * User: adit
 * Date: 6/20/19
 * Time: 4:10 AM
 */

namespace App\Models;


use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class RoleUserModel extends Model
{
    use Uuids;

    protected $table='role_user';
    protected $primaryKey='uuid';
    public $incrementing=false;
    public $timestamps=false;
}