<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class UserLevelModel extends Model
{
    use Uuids;

    protected $table='user_level';
    public $incrementing = false;
    protected $primaryKey = 'uuid';
}
