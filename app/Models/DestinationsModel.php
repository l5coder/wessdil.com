<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class DestinationsModel extends Model
{
    use Uuids;

    protected $table= 'destinations';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'uuid';
}
