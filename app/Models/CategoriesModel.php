<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class CategoriesModel extends Model
{
    use Uuids;

    protected $table='categories';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = 'uuid';
}
