<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryGroupsModel extends Model
{
    protected $table = 'product_category_groups';
    public $timestamps = false;
    public $incrementing = false;

}
