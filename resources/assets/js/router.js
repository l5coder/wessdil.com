import AdminContent from './MainAdmin'
import AdminAuth from './views/MainAuth'

//main continer frontend
import FrontEnd from './FrontEnd'

import Home from './views/front/Home'
import Tour from './views/front/Tour'
import Rentcar from './views/front/Rentcar'
import Hotel from './views/front/Hotel'
import Shop from './views/front/Shop'
import HotelDetail from './views/front/HotelDetail'
import HotelInput from './views/front/HotelInput'
import FormPembelian from './views/front/FormPembelian'
import TourDetail from './views/front/TourDetail'
import Destination from './views/front/Destination'
import Payment from './views/front/Payment'
import Cart from './views/front/Cart'
import PemesananTour from './views/front/PemesananTour'
import Faq from './views/front/Faq'
import Profile from './views/front/Profile'
import ChangePwd from './views/front/ChangePassword'
import Order from './views/front/Order'
import Refund from './views/front/Refund'


//end frontend

//main container backend
import BackEnd from './BackEnd'

//auth
import Login from './views/Login'
import User from './views/Users'
import IndexUser from './views/users/index'
import CreateUser from './views/users/create'
import EditUser from './views/users/edit'
import ChangePassword from './views/users/ChangePassword'

//level
import UserLevel from './views/UserLevel'
import IndexUserLevel from './views/userlevels/index'
import CreateUserLevel from './views/userlevels/create'
import EditUserLevel from './views/userlevels/edit'

//cities
import Cities from './views/Cities'
import IndexCity from './views/cities/index'
import CreateCity from './views/cities/create'
import EditCity from './views/cities/edit'

//tour package category
import TourPackageCategories from './views/TourPackageCategories'
import IndexTourPackageCategory from './views/tourpackagecategories/index'
import CreateTourPackageCategory from './views/tourpackagecategories/create'
import EditTourPackageCategory from './views/tourpackagecategories/edit'

//tours
import Tours from './views/Tours'
import IndexTour from './views/tours/index'
import CreateTour from './views/tours/create'
import EditTour from './views/tours/edit'

//dashboard
import Dashboard from './views/Dasboard'
import CompanyProfile from "./views/front/CompanyProfile";
import PrivacyPolicy from "./views/front/PrivacyPolicy";
import TermsAndConditions from './views/front/TermsAndConditions';


const router = [
    {
        path:'/',
        component:FrontEnd,
        children:[
            {
                path:'',
                component:Home,
                name:'home'
            },
            {
                path:'tour',
                component:Tour,
                name:'tour'
            },
            {
                path:'rentcar',
                component:Rentcar,
                name:'rentcar'
            },
            {
                path:'hotel',
                component:Hotel,
                name:'hotel'
            },
            {
                path:'shop',
                component:Shop,
                name:'shop'
            },
            {
                path:'hoteldetail',
                component:HotelDetail,
                name:'hoteldetail'
            },
            {
                path:'hotelinput',
                component:HotelInput,
                name:'hotelinput'
            },
            {
                path:'book',
                component:PemesananTour,
                name:'book'
            },
            {
                path:'book/payment',
                component:Payment,
                name:'payment'
            },
            {
                path:'formpembelian',
                component:FormPembelian,
                name:'formpembelian'
            },
            {
                path:':destinationslug/:productslug',
                component:TourDetail,
                name:'tourdetail'
            },
            {
                path:':slug',
                component:Destination,
                name:'destination'
            },

            {
                path:'cart',
                component:Cart,
                name:'cart'
            },
            {
                path:'faq',
                component:Faq,
                name:'faq'
            },
            {
                path:'profile',
                component:Profile,
                name:'profile'
            },
            {
                path:'changepassword',
                component:ChangePwd,
                name:'changepassword'
            },
            {
                path:'myorder',
                component:Order,
                name:'myorder'
            },
            {
                path:'myrefund',
                component:Refund,
                name:'myrefund'
            },
            {
                path:'companyprofile',
                component:CompanyProfile,
                name:'companyprofile'
            },
            {
                path:'privacypolicy',
                component:PrivacyPolicy,
                name:'privacypolicy'
            },
            {
                path:'termscondition',
                component:TermsAndConditions,
                name:'termscondition'
            },
        ],
        meta:{
            auth:false
        }
    },
    {
        path:'/login',
        component:AdminAuth,
        children:[
            {
                path:'',
                component:Login,
                name:'login'
            }
        ],
        meta:{
            auth:false
        }
    },
    {
        path:'/back-office',
        component:AdminContent,
        meta:{
            auth:true
        },
        children:[
            {
                path:'',
                component:Dashboard,
                name:'dashboard'
            },
            {
                path:'user',
                component:User,
                children:[
                    {
                        path:'',
                        component:IndexUser,
                        name:'indexUser'
                    },
                    {
                        path:'create',
                        component:CreateUser,
                        name:'createUser'
                    },
                    {
                        path:':id/edit',
                        component:EditUser,
                        name:'editUser'
                    },
                    {
                        path:'change-password',
                        component:ChangePassword,
                        name:'changePassword'
                    }
                ]
            },
            {
                path:'level-pengguna',
                component:UserLevel,
                children:[
                    {
                        path:'',
                        component:IndexUserLevel,
                        name:'indexUserLevel'
                    },
                    {
                        path:'create',
                        component:CreateUserLevel,
                        name:'createUserLevel'
                    },
                    {
                        path:':id/edit',
                        component:EditUserLevel,
                        name:'editUserLevel'
                    }
                ]
            },
            {
                path:'city',
                component:Cities,
                children:[
                    {
                        path:'',
                        component:IndexCity,
                        name:'indexCity'
                    },
                    {
                        path:'create',
                        component:CreateCity,
                        name:'createCity'
                    },
                    {
                        path:':uuid/edit',
                        component:EditCity,
                        name:'editCity'
                    }
                ]
            },
            {
                path:'tour-package-category',
                component:TourPackageCategories,
                children:[
                    {
                        path:'',
                        component:IndexTourPackageCategory,
                        name:'indexTourPackageCategory'
                    },
                    {
                        path:'create',
                        component:CreateTourPackageCategory,
                        name:'createTourPackageCategory'
                    },
                    {
                        path:':uuid/edit',
                        component:EditTourPackageCategory,
                        name:'editTourPackageCategory'
                    }
                ]
            },
            {
                path:'tour',
                component:Tours,
                children:[
                    {
                        path:'',
                        component:IndexTour,
                        name:'indexTour'
                    },
                    {
                        path:'create',
                        component:CreateTour,
                        name:'createTour'
                    },
                    {
                        path:':uuid/edit',
                        component:EditTour,
                        name:'editTour'
                    }
                ]
            }
        ]
    }

]


export default router