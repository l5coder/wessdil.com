import Vue from 'vue'
import Vuex from 'vuex'
import loading from 'vue-full-loading'
import Select2 from 'v-select2-component'
import {mapActions,mapGetters} from 'vuex'
import PageHeader from './components/PageHeader'
import vue2Dropzone from 'vue2-dropzone'
Vue.use(Vuex)


export const components = {
    loading,
    'select2':Select2,
    'page-header':PageHeader,
    vueDropzone:vue2Dropzone
}

export const computed = {
    ...mapGetters('BaseStore',{
        labelLoading:'getLoadingMessage',
        isLoadingShow:'getIsLoadingShow'
    })
}

export const methods = {
    getDateNowFormated(){
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
}