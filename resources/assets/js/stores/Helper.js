import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:8000/api'
class Helper {
    browse(url){
        return axios.get(url)
    }

    read(url){
        return axios.get(url)
    }

    edit(url,param){
        return axios.put(url,param)
    }

    add(url,param){
        return axios.post(url,param)
    }

    delete(url){
        return axios.delete(url)
    }
}

export default new Helper();