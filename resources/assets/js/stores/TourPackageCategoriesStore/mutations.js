import * as types from '../mutation-types';

export default {
    [types.BROWSE](state, { data }) {
        state.tourPackageCategories = data;
    },
    [types.READ](state,{index,data}){
        if(index == null){
            state.tourPackageCategory = data
        }else{
            state.tourPackageCategory = state.tourPackageCategories[index]
        }
    },
    [types.EDIT](state,{index,data}){
        state.tourPackageCategories[index] = data
    },
    [types.ADD](state,{data}){
        state.tourPackageCategories.push(data)
    },
    [types.DELETE](state,{index}){
        state.tourPackageCategories.splice(index,1)
    }
};
