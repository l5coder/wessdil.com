export function getTourPackageCategories(state) {
    return state.tourPackageCategories;
}

export function getTourPackageCategory(state) {
    return state.tourPackageCategory;
}
