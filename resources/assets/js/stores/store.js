import Vue from 'vue'
import Vuex from 'vuex'
import BaseStore from './Base/'
import UserStore from './UserStore'
import UserLevelStore from './UserLevelStore'
import CitiesStore from './CitiesStore'
import ToursStore from './ToursStore'
import TourPackageCategoriesStore from './TourPackageCategoriesStore'
import DestinationStore from './DestinationStore'
import ProductStore from './ProductStore'


Vue.use(Vuex)

export default new Vuex.Store({
  modules:{
    'BaseStore':BaseStore,
    'UserStore':UserStore,
    'UserLevelStore':UserLevelStore,
    'CitiesStore':CitiesStore,
    'ToursStore':ToursStore,
    'TourPackageCategoriesStore':TourPackageCategoriesStore,
    'DestinationStore':DestinationStore,
    'ProductStore':ProductStore
  }
})
