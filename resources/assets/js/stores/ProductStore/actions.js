import * as types from '../mutation-types';
import ApiHelper from "../ApiHelper";

export async function browse({dispatch, commit, rootGetters},{url}) {
    let isLoading = true
    let loadingMessage = 'Loading data..'
    dispatch('BaseStore/changeLoadingState', {isLoading: isLoading, labelLoading: loadingMessage}, {root: true})

    return new Promise((resolve,reject)=>{
        return ApiHelper.browse(url).then((response) => {
            let results = [];
            results = response
            commit(types.BROWSE, {data: results})
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            resolve('data successfully loaded')
        }).catch((error) => {
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            reject(error)
        })
    })
}

export async function pagination({dispatch,commit,rootGetters},{url}) {
    let isLoading = true
    let loadingMessage = 'Loading data...'
    dispatch('BaseStore/changeLoadingState', {isLoading: isLoading, labelLoading: loadingMessage}, {root: true})
    return new Promise((resolve,reject)=>{
        return ApiHelper.browse(url).then((response)=>{
            console.log(response)
            let items = response
            dispatch('PaginationStore/setItems',{data:items},{root:true})
        }).catch((error)=>{
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            reject(error)
        })
    })
}

export function read({dispatch, commit, rootGetters}, {id}) {
    let isLoading = true
    let loadingMessage = 'Loading data...'
    dispatch('BaseStore/changeLoadingState', {isLoading: isLoading, labelLoading: loadingMessage}, {root: true})

    return new Promise((resolve,reject)=>{
        let url = 'product/'+id
        return ApiHelper.read(url).then((response) => {
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            commit(types.READ, {data: response})
            resolve('Data successfully loaded')
        }).catch((error) => {
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            reject(error)
        })
    })
}

export function edit({dispatch, commit, rootGetters}, {input}) {
    let isLoading = true
    let loadingMessage = 'Saving data, please wait...'
    let url = 'product/'+input.uuid

    dispatch('BaseStore/changeLoadingState', {isLoading: isLoading, labelLoading: loadingMessage}, {root: true})
    return new Promise((resolve, reject) => {
        ApiHelper.edit(url, input).then(() => {
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            commit(types.EDIT, {data: input})
            resolve('Data successfully saved')
        }).catch((error) => {
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            reject(error)
        })
    })
}

export async function add({dispatch, commit, rootGetters}, {input}) {
    let isLoading = true
    let loadingMessage = 'Saving data, please wait...'
    let url = 'product'

    dispatch('BaseStore/changeLoadingState', {isLoading: isLoading, labelLoading: loadingMessage}, {root: true})
    return new Promise((resolve,reject)=>{
        return ApiHelper.add(url, input).then((response) => {
            console.log(response)
            commit(types.ADD, {data: input})
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            resolve('Data successfully saved')
        }).catch((error) => {
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            reject(error)
        })
    })
}

export async function del({dispatch, commit, rootGetters}, {id}) {
    let isLoading = true
    let loadingMessage = 'Deleting data, please wait...'
    let url = 'product/'+id

    dispatch('BaseStore/changeLoadingState', {isLoading: isLoading, labelLoading: loadingMessage}, {root: true})

    return new Promise((resolve, reject) => {
        ApiHelper.delete(url)
            .then(() => {
                dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
                dispatch('pagination',{url:'/product'})
                resolve('Data successfully deleted')
            }).catch((error) => {
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            reject(error)
        })
    })
}

export function readSlug({dispatch, commit, rootGetters}, {url}) {
    let isLoading = true
    let loadingMessage = 'Loading data...'
    dispatch('BaseStore/changeLoadingState', {isLoading: isLoading, labelLoading: loadingMessage}, {root: true})

    return new Promise((resolve,reject)=>{
        return ApiHelper.read(url).then((response) => {
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            commit(types.READ, {data: response})
            resolve('Data successfully loaded')
        }).catch((error) => {
            dispatch('BaseStore/changeLoadingState', {isLoading: false, labelLoading: ''}, {root: true})
            reject(error)
        })
    })
}