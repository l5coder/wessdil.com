import * as types from '../mutation-types';

export default {
    [types.BROWSE](state, { data }) {
        state.products = data;
    },
    [types.READ](state,{data}){
        state.product = data
    },
    [types.EDIT](state, {data,index}){
        state.products[index] = data
    },
    [types.ADD](state,{data}){
        state.products.push(data)
    },
    [types.DELETE](state,{index}){
        state.products.splice(index,1)
    }
};
