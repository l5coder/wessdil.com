export function getCities(state) {
    return state.cities;
}

export function getCity(state) {
    return state.city;
}
