import * as types from '../mutation-types';

export default {
    [types.BROWSE](state, { data }) {
        state.cities = data;
    },
    [types.READ](state,{index,data}){
        if(index == null){
            state.city = data
        }else{
            state.city = state.cities[index]
        }
    },
    [types.EDIT](state,{index,data}){
        state.cities[index] = data
    },
    [types.ADD](state,{data}){
        state.cities.push(data)
    },
    [types.DELETE](state,{index}){
        state.cities.splice(index,1)
    }
};
