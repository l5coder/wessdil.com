import axios from 'axios'
class ApiHelper {
    async browse(url){
        return new Promise((resolve,reject)=>{
            return axios.get(url).then((response)=>{
                resolve(response.data.data)
            }).catch((error)=>{
                reject(error.response)
            })
        })
    }

    async read(url){
        return new Promise((resolve,reject)=>{
            return axios.get(url).then((response)=>{
                resolve(response.data.data)
            }).catch((error)=>{
                reject(error.response.data.message)
            })
        })
    }

    async add(url,param){
        return new Promise((resolve,reject)=>{
            return axios.post(url,param).then((response)=>{
                resolve(response.data)
            }).catch((error)=>{
                reject(error.response.data.message)
            })
        })
    }

    async edit(url,param){
        return new Promise((resolve,reject)=>{
            return axios.put(url,param,{
                headers:{
                    'Content-Type': 'application/json'
                }
            }).then((response)=>{
                resolve(response.data)
            }).catch((error)=>{
                reject(error.response.data.message)
            })
        })

    }

    async delete(url){
        return new Promise((resolve,reject)=>{
            return axios.delete(url,{
                headers:{
                    'Content-Type': 'application/json',
                    'Accept':'application/json'
                }
            }).then((response)=>{
                resolve(response.data)
            }).catch((error)=>{
                reject(error.response.data.message)
            })
        })
    }
}

export default new ApiHelper()

