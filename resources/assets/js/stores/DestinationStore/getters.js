export function getDestination(state) {
    return state.destination
}

export function getDestinations(state) {
    return state.destinations
}

export function getDestinationType(state) {
    return state.destinationType
}
