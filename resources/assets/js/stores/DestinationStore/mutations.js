import * as types from '../mutation-types';

export default {
    [types.BROWSE](state, { data }) {
        state.destinations = data;
    },
    [types.READ](state,{data}){
        state.destination = data
    },
    [types.EDIT](state, {data,index}){
        state.destinations[index] = data
    },
    [types.ADD](state,{data}){
        state.destinations.push(data)
    },
    [types.DELETE](state,{index}){
        state.destinations.splice(index,1)
    }
};
