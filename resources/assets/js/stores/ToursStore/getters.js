export function getTour(state) {
    return state.tour;
}

export function getTours(state) {
    return state.tours;
}
