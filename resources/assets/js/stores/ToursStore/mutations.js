import * as types from '../mutation-types';

export default {
    [types.BROWSE](state, { data }) {
        state.tours = data;
    },
    [types.READ](state,{index,data}){
        if(index == null){
            state.tour = data
        }else{
            state.tour = state.tours[index]
        }
    },
    [types.EDIT](state,{index,data}){
        state.tours[index] = data
    },
    [types.ADD](state,{data}){
        state.tours.push(data)
    },
    [types.DELETE](state,{index}){
        state.tours.splice(index,1)
    }
};
