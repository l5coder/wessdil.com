<?php

use Illuminate\Http\Request;


Route::post('login', 'AuthController@login');
Route::get('/one-day-tours','OneDayToursController@showPagingFrontOffice');
Route::get('/public/destination/{slug}','DestinationController@read');
Route::get('/public/destination','DestinationController@showAll');
Route::get('/public/product/{destinationUuid}','ProductController@getProductByDestination');
Route::get('/public/product/detail/{slug}','ProductController@read');
Route::get('/public/product','ProductController@showAll');

Route::group(['middleware'=>'auth:api'],function (){
    Route::post('logout', 'AuthController@logout');
    Route::get('auth/refresh', 'AuthController@refresh');
    Route::get('auth/user', 'AuthController@me');

    Route::post('/upload-file','UploadHandlerController@uploadHandlerImage');
    Route::get('/options/category','CategoryController@getCategoryOptions');
    Route::get('/options/destination','DestinationController@getDestinationOptions');
    Route::get('/options/vendor','VendorsController@getVendorOptions');

    Route::get('/user','UserController@showAll');
    Route::post('/user/create','UserController@create');
    Route::get('/user/{id}','UserController@read');
    Route::post('/user/update','UserController@update');
    Route::post('/user/{id}/delete','UserController@delete');
    Route::post('/user/update-profle','UserController@updateProfile');

    Route::get('/user-level','UserLevelController@showAll');
    Route::post('/user-level/create','UserLevelController@create');
    Route::get('/user-level/{id}','UserLevelController@read');
    Route::post('/user-level/update','UserLevelController@update');
    Route::post('/user-level/{id}/delete','UserLevelController@delete');

    Route::get('/city','CitiesController@showAll');
    Route::post('/city','CitiesController@create');
    Route::get('/city/{uuid}','CitiesController@read');
    Route::put('/city/{uuid}','CitiesController@update');
    Route::delete('/city/{uuid}','CitiesController@delete');
    Route::get('/city/by-type','CitiesController@getCityByCityType');
    Route::get('/country','CitiesController@getCountry');

    Route::post('/destination','DestinationController@create');
    Route::get('/destination','DestinationController@showAll');
    Route::get('/destination/type','DestinationController@getDestinationType');
    Route::get('/destination/{uuid}','DestinationController@read');
    Route::put('/destination/{uuid}','DestinationController@update');
    Route::delete('/destination/{uuid}','DestinationController@delete');

    Route::post('/category','CategoryController@create');
    Route::get('/category','CategoryController@showAll');
    Route::get('/category/treeview','CategoryController@categoryTreeview');
    Route::get('/category/{uuid}','CategoryController@read');
    Route::put('/category/{uuid}','CategoryController@update');
    Route::delete('/category/{uuid}','CategoryController@delete');

    Route::post('/product','ProductController@create');
    Route::get('/product','ProductController@showAll');
    Route::get('/product/{uuid}','ProductController@read');
    Route::put('/product/{uuid}','ProductController@update');
    Route::delete('/product/{uuid}','ProductController@delete');

    Route::post('/vendor','VendorsController@create');
    Route::get('/vendor','VendorsController@showAll');
    Route::get('/vendor/type','VendorsController@getVendorType');
    Route::get('/vendor/{uuid}','VendorsController@read');
    Route::put('/vendor/{uuid}','VendorsController@update');
    Route::delete('/vendor/{uuid}','VendorsController@delete');

    Route::post('tour/tiny-mce-upload-handler','ToursController@tinyMceUploadHandler');
});